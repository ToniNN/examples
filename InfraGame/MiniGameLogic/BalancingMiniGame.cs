﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BalancingMiniGame : MonoBehaviour, IMiniGame {

	//public variables for dependencies and debugging in Editor
	public Text timerText;
	public Image timerImage;
	public RoundTimer timer;
	public float forceModifier;
	public Text debugText;

	private Rigidbody2D m_upperBodyRB2D;
	private bool m_isCompleted;
	private float m_helperTimer;
	private float m_intervalForSound = 1.0f;
	private bool m_balancingFailed;
	private GameObject m_upperBody;
	private float m_maxRotation = 45f;

	void Awake ()
	{
		Initialization ();
	}

	public void Initialization()
	{
		m_upperBody = GameObject.Find ("UpperBody");
		timer = GetComponent<RoundTimer>();
		timer.timerImage = timerImage;
		StartCoroutine(timer.StartTimer(GameManager.instance.currentRoundDuration));
		m_upperBodyRB2D = m_upperBody.GetComponent<Rigidbody2D>();
	}

	void Update ()
	{
		#if UNITY_EDITOR
		if (Input.GetButtonDown ("Fire1")) //korvataan input.accelerationilla mobiilille
		{
			m_upperBody.GetComponent<Rigidbody2D> ().AddForce(new Vector2(50f, 0));
		}
		if (Input.GetButtonDown ("Fire2")) //korvataan input.accelerationilla mobiilille
		{
			m_upperBody.GetComponent<Rigidbody2D> ().AddForce(new Vector2(-50f, 0));
		}
		if (m_upperBody.transform.rotation.z < -0.45f || m_upperBody.transform.rotation.z > 0.45f)
		{
			if (!m_balancingFailed)
			{
				m_upperBody.GetComponent<Rigidbody2D> ().freezeRotation = true;
				m_balancingFailed = true;
				m_isCompleted = true;
				GameManager.instance.RoundEndActions(false);
			}
		}
		#endif

		m_upperBodyRB2D.AddForce(new Vector2(Input.acceleration.x * forceModifier, 0));
		if (m_upperBody.transform.rotation.z < -0.45f || m_upperBody.transform.rotation.z > 0.45f)
		{
			if (!m_balancingFailed && !m_isCompleted)
			{
				m_upperBody.GetComponent<Rigidbody2D> ().freezeRotation = true;
				m_balancingFailed = true;
				m_isCompleted = true;
				GameManager.instance.RoundEndActions(false);
			}
		}
	}

	void OnEnable()
	{
		timer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		timer.OnTimerEnd -= RoundEnd;
	}

	public void RoundEnd()
	{
		m_upperBody.GetComponent<Rigidbody2D> ().freezeRotation = true;

		if (!m_isCompleted && !m_balancingFailed)
		{
			m_isCompleted = true;
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	public bool IsCompleted() 
	{
		return true;
	}
}
