﻿using UnityEngine;

/// <summary>
/// Asteroid logic
/// </summary>
public class Asteroid : MonoBehaviour {

	//variable to set the line renderers length, can be set in the editor
	public float lineLength;

	//readonly variable that returns the velocity of the rigidbody2D
	public Vector3 velocity { get { return m_rb.velocity; } }

	//reference to the Line Renderer component, set in the editor
	public LineRenderer m_lineRenderer;

	//Rigidbody2D
	private Rigidbody2D m_rb;

	//constants
	private const int MAX_VERTEX_COUNT = 2;

	/// <summary>
	/// Called when the asteroid is spawned
	/// </summary>
	void Awake()
	{
		m_rb = GetComponent<Rigidbody2D>();
	}

	/// <summary>
	/// Shows the asteroids trajectory
	/// </summary>
	/// <param name="velocity"></param>
	public void ShowTrajectory(Vector3 velocity)
	{
		m_lineRenderer.positionCount = MAX_VERTEX_COUNT;
		m_lineRenderer.SetPosition(0, transform.position);
		m_lineRenderer.SetPosition(1, transform.position + (velocity * lineLength));
	}

	/// <summary>
	/// Sets the velocity of the asteroids rigidbody component
	/// </summary>
	/// <param name="velocity"></param>
	public void SetVelocity(Vector3 velocity)
	{
		m_rb.velocity = velocity;
	}


}
