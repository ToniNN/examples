﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class SnowShovelingMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public MeshRenderer snowRenderer;
	public Material snowMat;
	public Transform shoveler;
	public Transform shovel;
	public SpriteRenderer shovelerSprite;
	public int radius;
	public Texture2D snowTexture;
	public Image timerImage;
	public AudioSource mowerSound;

	private float m_distanceTraveled;
	private float m_distanceToTravel = 0.5f;
	private Vector3 m_topLeftCornerOfThePlane;
	private float m_widthOfPlane;
	private int m_direction;
	private float m_maxBound;
	private float m_minBound;
	private Camera m_cam;
	private Color m_transparent = new Color(0f, 0f, 0f, 0f);
	private RoundTimer m_roundTimer;
	private bool m_gameOver;
	private float m_verticalStep;
	private Texture2D m_tex;

	void OnEnable()
	{
		m_roundTimer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		m_roundTimer.OnTimerEnd -= RoundEnd;
	}

	void Awake()
	{
		Initialization();
	}

	public void Initialization()
	{
		//sets the material for the texture
		Material mat = new Material(snowMat);
		m_tex = Instantiate(snowRenderer.material.mainTexture) as Texture2D;
		mat.mainTexture = m_tex;
		snowRenderer.material = mat;

		m_cam = Camera.main;
		m_direction = -1;
		m_topLeftCornerOfThePlane = new Vector3(snowRenderer.bounds.min.x, snowRenderer.bounds.max.y - 1, -0.5f);
		print(m_topLeftCornerOfThePlane);
		m_maxBound = snowRenderer.bounds.max.x;
		print(m_maxBound);
		m_minBound = snowRenderer.bounds.min.x;

		m_widthOfPlane = snowRenderer.bounds.size.x;

		GameManager.instance.DisableScoreBoard();
		m_roundTimer = GetComponent<RoundTimer>();
		m_roundTimer.OnTimerEnd += RoundEnd;
		m_roundTimer.timerImage = timerImage;
		StartCoroutine(m_roundTimer.StartTimer(GameManager.instance.currentRoundDuration));
		m_verticalStep = snowRenderer.bounds.size.y / 4;
	}

	void Update()
	{
		foreach(Touch touch in Input.touches)
		{
			if(touch.phase == TouchPhase.Began && !m_gameOver)
			{
				Move();
			}
		}

		//for using mouse in the editor
		#if UNITY_EDITOR
		if(Input.GetButtonDown("Fire1") && !m_gameOver)
		{
			Move();
			if (!m_gameOver)
				Move();
			if (!m_gameOver)
				Move();
		}
		#endif
	}

   

	public void Move()
	{
		shoveler.position += new Vector3(m_distanceToTravel, 0, 0);
		EraseTexture();

		if(shoveler.position.x > m_maxBound || shoveler.position.x < m_minBound)
		{
			LowerLevel();
			m_distanceToTravel *= m_direction;
		}
		Checker();
	}

	void Checker()
	{
		if (shoveler.position.y < -3 && shoveler.position.x < -5 && !m_gameOver)
		{
			mowerSound.Stop();
			GameManager.instance.RoundEndActions(true);
			m_gameOver = true;
		}

	}

	public void LowerLevel()
	{
		shoveler.position = new Vector3(shoveler.position.x, shoveler.position.y - m_verticalStep, shoveler.position.z);
		shoveler.localScale = new Vector3(shoveler.localScale.x * -1.0f, 0.7f, 1);
	}

	void EraseTexture()
	{
		RaycastHit hit;
		if (!Physics.Raycast(m_cam.ScreenPointToRay(m_cam.WorldToScreenPoint(shovel.position)), out hit))
			return;
		Renderer rend = hit.transform.GetComponent<Renderer> ();
		MeshCollider meshCollider = hit.collider as MeshCollider;
		if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
		{
			print("no hit");
			return;
		}
		m_tex = rend.material.mainTexture as Texture2D;
		float xCordTemp = 0;
		float yCordTemp = 0;

		Vector2 pixelUv = hit.textureCoord;
		xCordTemp = pixelUv.x;
		yCordTemp = pixelUv.y;

		xCordTemp *= m_tex.width;
		yCordTemp *= m_tex.height;

		int xCord = (int)xCordTemp;
		int yCord = (int)yCordTemp;

		CircleCutOutFromTexture(xCord, yCord, m_tex, m_transparent);
		m_tex.Apply();
	}

	void CircleCutOutFromTexture(int cx, int cy, Texture2D tex, Color col)
	{
		int x, y, px, nx, py, ny, d;
		for (x = 0; x <= radius; x++)
		{
			d = (int)Mathf.Ceil(Mathf.Sqrt(radius * radius - x * x));
			for (y = 0; y <= radius; y++) 
			{
				px = cx + x;
				nx = cx - x;
				py = cy + y;
				ny = cy - y;

				tex.SetPixel(px, py, col);
				tex.SetPixel(nx, py, col);

				tex.SetPixel(px, ny, col);
				tex.SetPixel(nx, ny, col);
			}
		}
	}

	

	public void RoundEnd()
	{
		if (!m_gameOver)
		{
			m_gameOver = true;
			mowerSound.Stop();
			GameManager.instance.RoundEndActions(IsCompleted());
		}
	}

	public bool IsCompleted()
	{
		return false;
	}
}
