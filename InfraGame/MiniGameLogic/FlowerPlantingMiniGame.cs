﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FlowerPlantingMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public Image timerImage;
	public RoundTimer timer;

	private GameObject m_currentlySelectedObject;
	private Vector3 m_offset;
	private Vector2 m_hitPoint;
	private int m_flowersNotPlanted = 3;
	private Vector3 newPos;
	private bool m_isCompleted;
	private BoxCollider2D m_hole;

	void OnEnable()
	{
		timer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		timer.OnTimerEnd -= RoundEnd;
	}

	void Awake ()
	{
		Initialization ();
	}

	public void Initialization()
	{
		timer = GetComponent<RoundTimer>();
		timer.timerImage = timerImage;
		StartCoroutine(timer.StartTimer(GameManager.instance.currentRoundDuration));
	}

	void Update()
	{
		CheckForInput();
	}

	void CheckForInput()
	{
		if (Input.GetButton ("Fire1")) 
		{	
			if(m_currentlySelectedObject == null)
			{
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit2D hit;
				hit = Physics2D.Raycast (ray.origin, ray.direction);

				if(hit.collider != null)
				{
					if(hit.collider.tag == "Flower" && m_currentlySelectedObject == null)
					{
						m_currentlySelectedObject = hit.collider.gameObject;
						m_hitPoint = hit.point;
						m_offset.x = hit.point.x - m_currentlySelectedObject.transform.position.x;
						m_offset.y = hit.point.y - m_currentlySelectedObject.transform.position.y;
					}
				}


			}

			if(m_currentlySelectedObject != null)
			{
				Drag();
			}

		}
		else if(Input.GetButtonUp("Fire1") && m_currentlySelectedObject != null)
		{
			CheckIfPlanted();
		}
	}

	void Drag()
	{
		Vector3 touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		Vector3 newPosition = new Vector3(touchPosition.x - m_offset.x, touchPosition.y - m_offset.y, 0);
		m_currentlySelectedObject.transform.position = newPosition;
	}

	void CheckIfPlanted()
	{
		if(m_currentlySelectedObject.GetComponent<BoxCollider2D>().IsTouching(m_hole))
		{
			Destroy(m_hole.gameObject);																//kuoppa piiloon
			m_currentlySelectedObject.transform.Find ("RootsSprite").gameObject.SetActive (false);	//kukan rootsit piiloon
			m_currentlySelectedObject.gameObject.GetComponent<BoxCollider2D> ().enabled = false;	//kukan collider piiloon
			m_currentlySelectedObject = null;
			m_flowersNotPlanted--;

			SoundManager.instance.PlaySound(SoundManager.Sounds.LetterRightPlaceOrFlowerPlanted);

			if(m_flowersNotPlanted <= 0 && !m_isCompleted)
			{
				m_isCompleted = true;
				GameManager.instance.RoundEndActions(true);
			}
		}
		else
		{
			m_currentlySelectedObject = null;
		}
	}

	public void SetHole(BoxCollider2D h)
	{
		this.m_hole = h;
	}

	public void RoundEnd()
	{
		if (!m_isCompleted)
		{
			m_isCompleted = true;
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	public bool IsCompleted() 
	{
		return false;
	}

}
