﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WindowMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public Image timerImage;
	public RoundTimer timer;

	private bool m_gameCompleted;

	void OnEnable()
	{
		timer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		timer.OnTimerEnd -= RoundEnd;
	}

	void Awake ()
	{
		Initialization ();
	}

	public void Initialization()
	{
		timer = GetComponent<RoundTimer>();
		timer.timerImage = timerImage;
		StartCoroutine(timer.StartTimer(GameManager.instance.currentRoundDuration));
	}

	void Update ()
	{
		if (Input.GetButtonDown ("Fire1") && !m_gameCompleted) 
		{
			m_gameCompleted = true;
			SoundManager.instance.PlaySound(SoundManager.Sounds.WindowBreaking);
			GetComponent<Animator> ().enabled = true;
			StartCoroutine(RoundEndDelayed(1.5f, false));
		}
	}

	public void RoundEnd() 
	{
		if (!m_gameCompleted) {
			m_gameCompleted = true;
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	public bool IsCompleted() 
	{
			return true;
	}

	IEnumerator RoundEndDelayed(float delay, bool completed)
	{
		yield return new WaitForSeconds(delay);
		GameManager.instance.RoundEndActions(false);
	}

}
