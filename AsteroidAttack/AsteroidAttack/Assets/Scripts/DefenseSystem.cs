﻿using UnityEngine;

/// <summary>
/// Determines if the asteroids come too close and should be destroyd
/// </summary>
public class DefenseSystem : MonoBehaviour {

	//Singleton
	public static DefenseSystem instance;
	
	//Public variables that can be set in the editor to test different scenarios in the game
	public float distanceToTravel;
	public float safetyZoneRadius;
	public float missileSpeed;
	public float asteroidSpeed;

	//Reference to the missile turret transform
	public Transform turret;

	//the prefab used to spawn the missiles
	public GameObject missilePrefab;

	//private class members and constants
	private float m_currentDestroyTime;
	private readonly Vector3 m_earthPosition = Vector3.zero;
	
	/// <summary>
	/// Called when the game starts
	/// </summary>
	void Awake()
	{
		//Singleton
		instance = this;
	}

	///<summary>
	///Called from spawning asteroids.
	///</summary>
	public void SituationAnalysis(Vector3 asteroidPosition, Vector3 asteroidVelocity, GameObject asteroid)
	{
		//asteroid has been detected, employ math to figure out the next move
		bool missileShouldBeFired = TrajectoryWithinSafetyZone(asteroidPosition, asteroidVelocity);

		//if asteroid is dangerous, intercept it with a missile
		if (missileShouldBeFired)
		{
			FireMissile(asteroidPosition, asteroidVelocity);
			Destroy(asteroid, m_currentDestroyTime);
		}
	}

	///<summary>
	///We use a vector that overshoots the position of the earth 
	///and calculate a perpendicular vector from the earth to that line. If the length of the
	///perpendicular line is less than the radius of the safety zone we know that the asteroid will
	///enter the safety zone </summary>
	public bool TrajectoryWithinSafetyZone(Vector3 asteroidPosition, Vector3 asteroidVelocity)
	{
		//position on the asteroids trajectory after certain distance, needed for trigonometry operations
		Vector3 asteroidPosAfterDistance = asteroidPosition + asteroidVelocity;

		Vector3 a = asteroidPosition;
		Vector3 b = asteroidPosAfterDistance;
		Vector3 c = m_earthPosition;

		float distanceFromCenterOfEarth = Mathf.Abs((c.x - a.x) * (-b.y + a.y) + (c.y - a.y) * (b.x - a.x))
			/ Mathf.Sqrt(Mathf.Pow(-b.y + a.y, 2) + Mathf.Pow(b.x - a.x, 2));
		
		//taking to account also if the asteroid is moving towards or away from earth
		if (distanceFromCenterOfEarth < safetyZoneRadius && ObjectIsMovingTowards(m_earthPosition, asteroidPosition, asteroidVelocity))
			return true;
		else
			return false;
	   
	}

	///<summary>
	///Determine if an object is moving towards or away from another object 
	///</summary>
	public bool ObjectIsMovingTowards(Vector3 object1, Vector3 object2, Vector3 object2Velocity)
	{
		var dir = object1 - object2;
		return Vector3.Dot(dir, object2Velocity) > 0;
	}

	///<summary>
	///Fires a missile towards a target taking velocity into account
	///</summary>
	public void FireMissile(Vector3 targetPosition, Vector3 targetVelocity)
	{
		//instantiates the missile
		GameObject missile = Instantiate(missilePrefab, m_earthPosition, Quaternion.identity);

		//get the velocity vector for the missile
		missile.GetComponent<Rigidbody2D>().velocity = CalculateMissileVelocity(targetPosition, targetVelocity);

		//calculates the rotation for the turret and missile sprites so that they face the target
		var eulers = new Vector3(0, 0, Mathf.Atan2(missile.GetComponent<Rigidbody2D>().velocity.y, missile.GetComponent<Rigidbody2D>().velocity.x) * Mathf.Rad2Deg);

		//apply the rotation to sprites
		missile.transform.Rotate(eulers);
		turret.rotation = Quaternion.identity;
		turret.Rotate(eulers);

		//set the missile object to be destroy after the intercept time has passed so it looks like the missile and asteroid destroy each other
		Destroy(missile, m_currentDestroyTime);
	}

	///<summary>
	///Calculates the velocity vector for the missile that intercepts the asteroid. 
	///Solves only the general case where missile has a greater speed than the asteroid
	///</summary>
	public Vector3 CalculateMissileVelocity(Vector3 asteroidPosition, Vector3 asteroidVelocity)
	{
		//time it takes for the missile to travel to the intercept point
		var timeToInterception = 0.0f;

		//direction vector to the asteroid from the earth where the turret is
		var directionToAsteroid = m_earthPosition - asteroidPosition;

		//distance to asteroid
		var distanceToAsteroidFromMissile = Vector3.Distance(m_earthPosition, asteroidPosition);

		//variables used in the quadratic solution
		var a = Mathf.Pow(missileSpeed, 2) - Mathf.Pow(asteroidSpeed,2);
		var b = 2 * Vector3.Dot(directionToAsteroid, asteroidVelocity);
		var c = -distanceToAsteroidFromMissile * distanceToAsteroidFromMissile;

		//two answers from quadratic solution
		var sqrtpart = (b * b) - (4 * a * c);
		var answer1 = ((-1) * b + Mathf.Sqrt(sqrtpart)) / (2 * a);
		var answer2 = ((-1) * b - Mathf.Sqrt(sqrtpart)) / (2 * a);

		//if both negative interception cant happen in the future
		if (answer1 < 0 && answer2 < 0)
			return Vector3.zero;

		//due to many possible interception points, choose the most suitable
		if (answer1 > 0 && answer2 > 0)
			timeToInterception = Mathf.Min(answer1, answer2);
		else
			timeToInterception = Mathf.Max(answer1, answer2);

		//time to interception used by the asteroid and missile sprites to destroy themselves at the interception point
		m_currentDestroyTime = timeToInterception;

		//position where asteroid and missile make contact
		var interceptPosition = asteroidPosition + asteroidVelocity * timeToInterception;

		//velocity vector to interceptPosition
		var velocityToInterceptPosition = (interceptPosition - m_earthPosition) / timeToInterception;

		//if solution within general case and the interception is possible
		if (velocityToInterceptPosition.x != float.NaN && velocityToInterceptPosition.y != float.NaN)
			return (interceptPosition - m_earthPosition) / timeToInterception;
		else
			return Vector3.zero;

	}
}
