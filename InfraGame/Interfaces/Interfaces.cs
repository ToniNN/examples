﻿using UnityEngine;
using System.Collections;

//per game implementations to enforce certain structure
public interface IMiniGame
{
	void Initialization();
	bool IsCompleted(); 
	void RoundEnd();
}
