﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TrashPickingMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public BoxCollider2D spawnArea;
	public List<GameObject> trashObjects = new List<GameObject>();
	public Collider2D dumpster;
	public GameObject trashPrefab;
	public Image timerImage;
	public int left = 0;
	public int maxTries;

	private Collider2D[] m_neighbours;
	private Vector3 m_newPos;
	private RoundTimer m_roundTimer;
	[SerializeField]
	private Sprite[] m_trashSprites;
	private bool m_isCompleted;
	private GameObject m_currentlySelectedObject; 
	private BoxCollider2D m_dumpster;
	private Camera m_camera;
	private Vector3 m_offset;
	private Vector2 m_hitPoint;
	private int amountOfTrash = 5;

	void OnEnable()
	{
		m_roundTimer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		m_roundTimer.OnTimerEnd -= RoundEnd;
	}

	void Start()
	{
		Initialization();
	}

	public void Initialization()
	{
		m_camera = Camera.main;	
		m_dumpster = GameObject.Find("Dumpster").GetComponent<BoxCollider2D>();
		m_currentlySelectedObject = null;

		GameManager.instance.DisableScoreBoard();
		SpawnTrash();
		m_roundTimer = GetComponent<RoundTimer>();
		m_roundTimer.OnTimerEnd += RoundEnd;
		m_roundTimer.timerImage = timerImage;
		StartCoroutine(m_roundTimer.StartTimer(GameManager.instance.currentRoundDuration));
	}

	void Update()
	{
		CheckForInput();
	}

	void CheckForInput()
	{
		if (Input.GetButton ("Fire1")) 
		{	
			if(m_currentlySelectedObject == null)
			{
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit2D hit;
				hit = Physics2D.Raycast (ray.origin, ray.direction);

				if(hit.collider != null)
				{
					if(hit.collider.tag == "Trash" && m_currentlySelectedObject == null)
					{
						m_currentlySelectedObject = hit.collider.gameObject;
						m_hitPoint = hit.point;
						m_offset.x = hit.point.x - m_currentlySelectedObject.transform.position.x;
						m_offset.y = hit.point.y - m_currentlySelectedObject.transform.position.y;
					}
				}


			}

			if(m_currentlySelectedObject != null)
			{
				Drag();
			}

		}
		else if(Input.GetButtonUp("Fire1") && m_currentlySelectedObject != null)
		{
			CheckIfInDumpster();
		}
	}

	void Drag()
	{
		Vector3 touchPosition = m_camera.ScreenToWorldPoint(Input.mousePosition);

		Vector3 newPosition = new Vector3(touchPosition.x - m_offset.x, touchPosition.y - m_offset.y, 0);
		m_currentlySelectedObject.transform.position = newPosition;
	}

	void CheckIfInDumpster()
	{
		if(m_currentlySelectedObject.GetComponent<CircleCollider2D>().IsTouching(m_dumpster) && GetComponent<TrashPickingGame>().left ==0)
		{
			Destroy(m_currentlySelectedObject.gameObject);
			m_currentlySelectedObject = null;

			amountOfTrash--;

			SoundManager.instance.PlaySound(SoundManager.Sounds.RightTrashOrTrashSorting);

			if(amountOfTrash == 0 && !m_isCompleted)
			{
				m_isCompleted = true;
				GameManager.instance.RoundEndActions(true);
			}
		}
		else
		{
			m_currentlySelectedObject = null;
		}
	}

	

	void SpawnTrash()
	{
		for(int i = 0; i < amountOfTrash; i++)
		{
			var trash = Instantiate(trashPrefab, GetRandomSpawnPoint(spawnArea.bounds), Quaternion.identity ) as GameObject;

			SpriteRenderer renderer = trash.GetComponent<SpriteRenderer>();
			renderer.sortingOrder = i + 2;
			renderer.sprite = m_trashSprites[Random.Range(0, m_trashSprites.Length - 1)];
			trashObjects.Add(trash);
		}
	}

	Vector3 GetRandomSpawnPoint(Bounds bounds)
	{
		Vector3 prefabSize = trashPrefab.GetComponent<SpriteRenderer>().bounds.size;
		Bounds currentBounds = bounds;

		float adjustedMinX = bounds.min.x + (prefabSize.x / 2);
		float adjustedMinY = bounds.min.y + (prefabSize.y / 2);
		float adjustedMaxX = bounds.max.x - (prefabSize.x / 2);
		float adjustedMaxY = bounds.max.y - (prefabSize.y / 2);
		for(int i = 0; i < maxTries; i++)
		{
			m_newPos = new Vector3(Random.Range(adjustedMinX, adjustedMaxX), Random.Range(adjustedMinY, adjustedMaxY), 0);
			if(CheckIfOtherObjectsNear(m_newPos, prefabSize.x))
			{
				continue;
			}
			else
			{
				return m_newPos;
			}
		}
		return m_newPos;
	}

	bool CheckIfOtherObjectsNear(Vector3 pos, float distance)
	{
		m_neighbours = Physics2D.OverlapCircleAll(pos, distance, 1<<9);
		if(m_neighbours.Length > 0)
		{
			return true;	
		}
		else
		{
			return false;
		}
	}

	public bool IsCompleted()
	{
		return false;
	}

	public void RoundEnd()
	{
		foreach(GameObject go in trashObjects)
		{
			if(go != null)
			{
				left++;
			}
		}
		if(left > 0 && !m_isCompleted)
		{
			m_isCompleted = true;
			GameManager.instance.RoundEndActions(IsCompleted());
		}
	}
}
