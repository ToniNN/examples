﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WalkingMiniGame : MonoBehaviour, IMiniGame {

    //public variables for debugging and dependencies in Editor
    public Image timerImage;
	public RoundTimer timer;

    private Animator m_anim;
	private bool m_stepping;
	private bool m_leftNext;
	private bool m_gameCompleted;
	private Vector3 m_currentPos;
	private Vector3 m_endPos;
	private float m_step = 1.5f;
	private float m_speed = 0f;
	private int m_stepsToFinish = 7;

    void OnEnable()
    {
        timer.OnTimerEnd += RoundEnd;
    }

    void OnDisable()
    {
        timer.OnTimerEnd -= RoundEnd;
    }

    void Awake ()
	{
		Initialization ();
	}

	public void Initialization()
	{
		m_currentPos = transform.position;
		m_endPos = new Vector3 (m_currentPos.x + m_step, m_currentPos.y);
		m_stepping = false;
		m_leftNext = false;

		RoundTimer.RoundDuration customDur = RoundTimer.RoundDuration.Eight;
		GameManager.Round currentRound = GameManager.instance.currentRound;
		switch(currentRound)
		{
		case GameManager.Round.First:
			customDur = RoundTimer.RoundDuration.Fourteen;
			break;

		case GameManager.Round.Second:
			customDur = RoundTimer.RoundDuration.Twelve;
			break;

		case GameManager.Round.Third:
			customDur = RoundTimer.RoundDuration.Twenty;
			break;

		case GameManager.Round.Fourth:
			customDur = RoundTimer.RoundDuration.Eight;
			break;
		}

		timer = GetComponent<RoundTimer>();
		timer.timerImage = timerImage;
		StartCoroutine(timer.StartTimer(customDur));
		m_anim = GetComponent<Animator> ();
	}


	void Update()
	{
		if(!m_gameCompleted)
		CheckForInput();
		if (m_stepping){
			m_speed += Time.deltaTime; 
			transform.parent.transform.position = Vector3.Lerp (m_currentPos, m_endPos, m_speed);
		}
	}

	void CheckForInput()
	{
		if (m_stepsToFinish == 0 && !m_gameCompleted)
		{
			GameManager.instance.RoundEndActions(true);
			m_gameCompleted = true;
		}
			
		else if (Input.GetButtonDown ("Fire1")) 
		{	
			if (m_stepping) {
				m_anim.Play ("fall");
				SoundManager.instance.PlaySound(SoundManager.Sounds.JennaFallsDownWalkingGame);
				StartCoroutine(Delay(1.5f,true));
			} else if (!m_leftNext) {
				m_anim.Play ("stepWithRightB");
				m_leftNext = true;
				SoundManager.instance.PlaySoundWithCustomPitch(SoundManager.Sounds.WalkingGameStep, 0.5f);
			} else if (m_leftNext) {
				m_anim.Play ("stepWithLeftB");
				m_leftNext = false;
				SoundManager.instance.PlaySoundWithCustomPitch(SoundManager.Sounds.WalkingGameStep, 0.5f);
			}
			m_stepsToFinish--;
		}
	}

	void IsStepping ()
    {
		if (m_stepping)
        {
			m_stepping = false;
			m_currentPos = transform.parent.transform.position;
			m_endPos = new Vector3 (m_currentPos.x + m_step, m_currentPos.y);
			m_speed = 0f;
		}
        else
        {
			m_stepping = true;
		}
	}

	
	public void RoundEnd()
	{
		if (!m_gameCompleted) {
			m_gameCompleted = true;
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	public bool IsCompleted() 
	{
		return false;
	}

	IEnumerator Delay(float delay, bool completed)
	{
		m_gameCompleted = true;
		yield return new WaitForSeconds(delay);
		GameManager.instance.RoundEndActions(false);
	}

	public void PlayStepSound()
	{
		SoundManager.instance.PlaySound(SoundManager.Sounds.WalkingGameStep);
	}

}
