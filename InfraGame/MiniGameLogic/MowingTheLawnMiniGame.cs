﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class MowingTheLawnMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public MeshRenderer grassRenderer;
	public Transform shoveler;
	public Transform shovel;
	public SpriteRenderer shovelerSprite;
	public int radius;
	public Texture2D grassTexture;
	public AudioSource mowerSound;
	public Material grassMat;
	public Image timerImage;

	private float m_distanceTraveled;
	private float m_distanceToTravel = 0.5f;
	private Vector3 m_topLeftCornerOfThePlane;
	private float m_widthOfPlane;
	private int m_direction;
	private float m_maxBound;
	private float m_minBound;
	private Camera m_cam;
	private Color m_transparent = new Color(0f, 0f, 0f, 0f);
	private RoundTimer m_roundTimer;
	private bool m_gameOver;
	private float m_verticalStep;
	private Texture2D m_tex;
	

	public void Initialization()
	{
	}

	void Awake()
	{
		Material mat = new Material(grassMat);
		m_tex = Instantiate (grassRenderer.material.mainTexture) as Texture2D;
		mat.mainTexture = m_tex;
		grassRenderer.material = mat;

		m_verticalStep = grassRenderer.bounds.size.y / 3;
		m_cam = Camera.main;
		m_direction = -1;
		var shovelerSize = shoveler.GetComponent<SpriteRenderer>().bounds.size;
		m_topLeftCornerOfThePlane = new Vector3(grassRenderer.bounds.min.x, grassRenderer.bounds.max.y - (shovelerSize.y / 2), -0.5f);
		m_maxBound = grassRenderer.bounds.max.x;
		m_minBound = grassRenderer.bounds.min.x;

		m_widthOfPlane = grassRenderer.bounds.size.x - (shovelerSize.x / 2);

		GameManager.instance.DisableScoreBoard();
		m_roundTimer = GetComponent<RoundTimer>();
		m_roundTimer.OnTimerEnd += RoundEnd;
		m_roundTimer.timerImage = timerImage;
		StartCoroutine(m_roundTimer.StartTimer(GameManager.instance.currentRoundDuration));
	}

	public void Move()
	{
		shoveler.position += new Vector3(m_distanceToTravel, 0, 0);
		EraseTexture();
		SoundManager.instance.PlaySound(SoundManager.Sounds.Swoosh);

		if (shoveler.position.x > m_maxBound || shoveler.position.x < m_minBound)
		{
			LowerLevel();
			m_distanceToTravel *= m_direction;
		}
		Checker();
	}

	public void LowerLevel()
	{
		shoveler.position = new Vector3(shoveler.position.x, shoveler.position.y - m_verticalStep, shoveler.position.z);
		shovelerSprite.flipX = !shovelerSprite.flipX;
	}

	void Update()
	{
		foreach(Touch touch in Input.touches)
		{
			if(touch.phase == TouchPhase.Began && !m_gameOver)
			{
				Move();
			}
		}
		#if UNITY_EDITOR
		if(Input.GetButtonDown("Fire1") && !m_gameOver)
		{
			Move();
			if (!m_gameOver)
				Move();
			if (!m_gameOver)
				Move();
		}
		#endif
	}

	public void RoundEnd()
	{
		if (!m_gameOver) 
		{
			m_gameOver = true;
			mowerSound.Stop();
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	public bool IsCompleted()
	{
		return false;
	}
	
	void Checker()
	{
		if (shoveler.position.y < -3.2f && shoveler.position.x > 3.8 && !m_gameOver)
		{
			m_gameOver = true;
			mowerSound.Stop();
			GameManager.instance.RoundEndActions(true);
		}

	}

	void EraseTexture()
	{
		RaycastHit hit;
		if (!Physics.Raycast(m_cam.ScreenPointToRay(m_cam.WorldToScreenPoint(shovel.position)), out hit))
			return;

		Renderer rend = hit.transform.GetComponent<Renderer>();
		MeshCollider meshCollider = hit.collider as MeshCollider;
		if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
		{
			print("no hit");
			return;
		}


		m_tex = rend.material.mainTexture as Texture2D;

		float xCordTemp = 0;
		float yCordTemp = 0;

		Vector2 pixelUv = hit.textureCoord;
		xCordTemp = pixelUv.x;
		yCordTemp = pixelUv.y;

		xCordTemp *= m_tex.width;
		yCordTemp *= m_tex.height;

		int xCord = (int)xCordTemp;
		int yCord = (int)yCordTemp;

		CircleCutOutFromTexture(xCord, yCord, m_tex, m_transparent);

		m_tex.Apply();
	}

	void CircleCutOutFromTexture(int cx, int cy, Texture2D tex, Color col)
	{
		int x, y, px, nx, py, ny, d;

		for (x = 0; x <= radius; x++)
		{
			d = (int)Mathf.Ceil(Mathf.Sqrt(radius * radius - x * x));
			for (y = 0; y <= radius; y++) // radius = nelikulmio, d = ympyrä
			{
				px = cx + x;
				nx = cx - x;
				py = cy + y;
				ny = cy - y;

				tex.SetPixel(px, py, col);
				tex.SetPixel(nx, py, col);

				tex.SetPixel(px, ny, col);
				tex.SetPixel(nx, ny, col);

			}
		}
	}

	void OnDisable()
	{
		m_roundTimer.OnTimerEnd -= RoundEnd;
	}
}
