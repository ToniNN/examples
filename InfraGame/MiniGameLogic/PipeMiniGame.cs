﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PipeMiniGame : MonoBehaviour, IMiniGame, IBeginDragHandler, IPointerClickHandler {

	//public variables for debugging and dependencies in Editor
	public float startWidth = 1.0f;
	public float endWidth = 1.0f;
	public float threshold = 0.001f;
	public int amountOfPipesBroken;
	public Text timerText;
	public RoundTimer timer;

	private Camera m_cam;
	private int m_lineCount = 0;
	private Material m_pipeMaterial;
	private Vector3 m_startPos;
	private Transform m_parent;
	private bool m_collider1Hit, m_collider2Hit;
	private bool m_renderLine;
	private int m_parentId;
	private int m_colliderHitId;
	private bool m_startRaycasting;
	private int m_pipesFixed;
	private Vector2 m_referenceResolution = new Vector2(944, 590);
	private Vector3[] m_positions;
	private bool m_completed;
	private LineRenderer m_lineRenderer;
	private List<Vector3> m_linePoints = new List<Vector3>();
	private Vector3 m_lastPos = Vector3.one * float.MaxValue;

	void onEnable()
	{
		timer.OnTimerEnd += RoundEnd;
	}
	void onDisable()
	{
		timer.OnTimerEnd -= RoundEnd;
	}

	void Awake()
	{
		Initialization();
	}

	public void Initialization()
	{
		m_lineRenderer = GetComponent<LineRenderer>();
		m_cam = Camera.main;

		timer = GetComponent<RoundTimer>();
		timer.timerText = timerText;
		timer.OnTimerEnd += RoundEnd;
		StartCoroutine(timer.StartTimer(GameManager.instance.currentRoundDuration));
		m_startRaycasting = true;
	}

	void Update()
	{
		if (Input.GetButton("Fire1") && m_startRaycasting)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D hit;
			hit = Physics2D.Raycast(ray.origin, ray.direction);

			if(hit.collider != null)
			{
				print(hit.collider.name);
				if(hit.collider.tag == "PipeCollider" && hit.transform.parent.GetComponent<BrokenPipe>().isFixed == false)
				{
					m_renderLine = true;

					if (m_parentId == 0)
					{
						m_parentId = hit.transform.parent.GetInstanceID();
						m_colliderHitId = hit.collider.GetInstanceID();
						print(m_parentId + " " + m_colliderHitId);
						Debug.Log("hit");
					}
					else if (hit.collider.GetInstanceID() != m_colliderHitId && m_parentId == hit.transform.parent.GetInstanceID())
					{
						Debug.Log("fix");
						hit.transform.parent.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
						m_startRaycasting = false;
						m_lineRenderer.enabled = false;
						hit.transform.parent.GetComponent<BrokenPipe>().isFixed = true;
						m_pipesFixed++;
						m_renderLine = false;
						m_parentId = 0;
						SoundManager.instance.PlaySound(SoundManager.Sounds.PipeFixed);

						if(m_pipesFixed == amountOfPipesBroken && !m_completed)
						{
							m_completed =true;
							GameManager.instance.RoundEndActions(true);
						}
					}

				}
				
			}

			if (m_renderLine)
			{
				m_lineRenderer.enabled = true;
				Vector3 mousePos = Input.mousePosition;
				mousePos.z = 1;
				Vector3 mouseWorld = m_cam.ScreenToWorldPoint(mousePos);

				if (m_startPos == Vector3.zero)
				{
					m_startPos = m_cam.ScreenToWorldPoint(mousePos);
				}

				UpdateLine();
			}

		}

		if (Input.GetButtonUp("Fire1"))
		{
			m_startPos = Vector3.zero;
			m_parent = null;
			m_startRaycasting = true;
			m_lineRenderer.enabled = false;
			m_renderLine = false;
			m_parentId = 0;
		}
		
	}


	void UpdateLine()
	{
		m_lineRenderer.SetWidth(startWidth, endWidth);
		m_lineRenderer.SetVertexCount(2);

		Vector3 mousePos = Input.mousePosition;
		mousePos.z = 1;
		Vector3 mouseWorld = m_cam.ScreenToWorldPoint(mousePos);

		m_lineRenderer.SetPosition(0, m_startPos);
		m_lineRenderer.SetPosition(1, mouseWorld);
		m_lineCount = m_linePoints.Count;
	}

	public bool IsCompleted()
	{
		return true;
	}

	public void RoundEnd()
	{
		if (!m_completed) {
			m_completed = true;
			GameManager.instance.RoundEndActions (false);
		}
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		print(eventData.pointerCurrentRaycast.gameObject.name);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		print(eventData.pointerCurrentRaycast.gameObject.name);
	}
}

