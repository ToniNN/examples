﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

//spawns randomly 2-5 icicles
//tap 4 times to break an icicle

public class IceClearingMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public Image timerImage;
	public RoundTimer timer;
	public BoxCollider2D spawnArea;
	public GameObject iciclePrefab;

	private int m_iciclesCount;
	private int m_iciclesDestroyed;
	private Vector3 m_newPos;
	private Collider2D[] m_neighbours;
	private int m_maxTries = 3;
	private bool m_isCompleted;

	void Start ()
	{
		Initialization ();
	}

	public void Initialization()
	{
		timer = GetComponent<RoundTimer>();
		timer.timerImage = timerImage;
		timer.OnTimerEnd += RoundEnd;
		StartCoroutine(timer.StartTimer(GameManager.instance.currentRoundDuration));
		m_iciclesCount = Random.Range (2, 5);
		m_iciclesDestroyed = 0;
		SpawnIcicles ();
	}

	void SpawnIcicles ()
	{
		for (int i = 0; i < m_iciclesCount; i++)
		{
			var icicle = Instantiate (iciclePrefab, GetRandomSpawnPoint (spawnArea.bounds), Quaternion.identity) as GameObject;
		}
	}

	Vector3 GetRandomSpawnPoint(Bounds bounds)
	{
		Vector3 prefabSize = iciclePrefab.GetComponent<SpriteRenderer>().bounds.size;
		Bounds currentBounds = bounds;

		float adjustedMinX = bounds.min.x;
		float adjustedMinY = bounds.min.y;
		float adjustedMaxX = bounds.max.x;
		float adjustedMaxY = bounds.max.y;

		for(int i = 0; i < m_maxTries; i++)
		{
			m_newPos = new Vector3(Random.Range(adjustedMinX, adjustedMaxX), Random.Range(adjustedMinY, adjustedMaxY), 0);

			if(CheckIfOtherObjectsNear(m_newPos, prefabSize.x))
				continue;
			else
				return m_newPos;
		}
		return m_newPos;
	}

	bool CheckIfOtherObjectsNear(Vector3 pos, float distance)
	{
		m_neighbours = Physics2D.OverlapCircleAll(pos, distance, 1<<9);
		if(m_neighbours.Length > 0)
		{
			return true;	
		}
		else
		{
			return false;
		}
	}

	void Update ()
	{
		if (Input.GetButtonDown ("Fire1")) 
		{	
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit2D hit;
			hit = Physics2D.Raycast (ray.origin, ray.direction);
			if (hit.collider != null) 
			{
				if (hit.collider.tag == "Icicle")
				{
					SoundManager.instance.PlaySound(SoundManager.Sounds.IcePickHit);

					if (hit.transform.gameObject.GetComponent<Icicle> ().ReduceHP () == false)
					{
						m_iciclesCount--;
						SoundManager.instance.PlaySound(SoundManager.Sounds.IcePickDestroyed);
					}
						
					if (m_iciclesCount <= 0 && !m_isCompleted) {
						m_isCompleted = true;
						GameManager.instance.RoundEndActions (true);
					}
				}
			}
		}
	}

	void OnDisable()
	{
		timer.OnTimerEnd -= RoundEnd;
	}

	public void RoundEnd()
	{
		if (!m_isCompleted) 
		{
			m_isCompleted = true;
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	public bool IsCompleted() 
	{
		return false;
	}

}
