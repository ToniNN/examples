﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Class for managing and checking shapes, used in the SandRoad and Trimming minigames

public class ShapeToucher : MonoBehaviour {

	//public variables for debugging and dependencies in Editor
	public GameObject[] shapes;
	public CircleCollider2D[] shapeColliders;
	public bool allCollidersTouched;
	public LineRenderer touchLine;
	public bool unifiedShape;
	public bool renderLine;
	public Transform particles;
	public bool branches;
	public bool touching;
	public float startLineWidth, endLineWidth;

	private List<GameObject> m_touchedObjects = new List<GameObject>();
	private int m_numberOfCollidersToTouch;
	private int m_numberOfCollidersTouched;
	private List<Vector3> m_drawPositions = new List<Vector3>();
	private bool m_secondColliderTouched;
	private bool m_renderParticles;
	

	void Start()
	{
		if(particles == null)
			m_renderParticles = false;
		else 
		{
			m_renderParticles = true;
			particles.gameObject.SetActive(false);
		}
			
	}

	public void SpawnShape(Vector3 position)
	{
		var shape = Instantiate(shapes[Random.Range(0, shapes.Length)], position, Quaternion.identity) as GameObject;

		shapeColliders = shape.GetComponentsInChildren<CircleCollider2D>();

		m_numberOfCollidersToTouch = shapeColliders.Length;
	}

	void Update()
	{
		if(Input.GetButton("Fire1"))
		{
			if(renderLine)
				DrawLine();

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit2D hit;
			hit = Physics2D.Raycast (ray.origin, ray.direction);

			if(m_renderParticles)
			{
				particles.gameObject.SetActive(true);
				var position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				position.z = -5;
				particles.position = position;
				particles.GetComponent<ParticleSystem>().Emit(2);
			}

			if(hit.collider != null)
			{
				if(hit.collider.tag == "ShapeCollider" && !m_touchedObjects.Contains(hit.collider.gameObject))
				{
					m_numberOfCollidersTouched++;

					m_touchedObjects.Add(hit.collider.gameObject);

					if(branches)
					{
						hit.collider.transform.GetChild(0).GetComponent<Rigidbody2D>().isKinematic = false;
						Destroy(hit.collider.transform.GetChild(0).gameObject, 1.0f);
					}

					if(m_numberOfCollidersTouched == m_numberOfCollidersToTouch)
					{
						allCollidersTouched = true;
					}

					if(m_numberOfCollidersTouched == 2 && m_secondColliderTouched == false && unifiedShape == true)
					{
						m_touchedObjects.RemoveAt(0);
						m_secondColliderTouched = true;
						m_numberOfCollidersTouched--;
					}
				}
			}

		}

	}

	void DrawLine()
	{
		var position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		position.z = -3;

		m_drawPositions.Add(position);

		touchLine.SetVertexCount(m_drawPositions.Count);

		touchLine.SetPositions(m_drawPositions.ToArray());

		touchLine.SetWidth(startLineWidth, endLineWidth);

	}
}
