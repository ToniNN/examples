﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;



public class CityDegenerationManager : MonoBehaviour {

	const int fromHappyToCleanInHours = 4;
	const int fromCleanToDirtyInHours = 8;
	const int fromDirtyToChaoticInHours = 8;
	const int totalHoursFromHappyToChaotic = 20;
	const int twentyHoursInSeconds = 72000;

	const float happyUpperLimit = 100.0f;
	const float cleanUpperLimit = 80.0f;
	const float dirtyUpperLimit = 40.0f;
	const float chaoticUpperLimit = 0.0f;

	const float chaoticDeGenAlpha = 0.7f;
	const float dirtyDeGenAlpha = 0.5f;
	const float cleanDeGenAlpha = 0.25f;
	const float happyDeGenAlpha = 0.0f;

	public Image keskustaBlackness;
	public Image kupittaaBlackness;
	public Image marttiBlackness;
	public Image satamaBlackness;

	const int numberOfCityParts = 4;

	DateTime lastDateWhenQuit;

	public CityPart[] cityparts;
	string[] cityPartNames = {"Martti", "Kupittaa", "Keskusta", "Satama"};

	public GameObject[] kupittaaChaoticObjects;
	public GameObject[] kupittaaDirtyObjects;
	public GameObject[] kupittaaCleanObjects;
	public GameObject[] kupittaaHappyObjects;
	public GameObject[] marttiChaoticObjects;
	public GameObject[] marttiDirtyObjects;
	public GameObject[] marttiCleanObjects;
	public GameObject[] marttiHappyObjects;
	public GameObject[] keskustaChaoticObjects;
	public GameObject[] keskustaDirtyObjects;
	public GameObject[] keskustaCleanObjects;
	public GameObject[] keskustaHappyObjects;
	public GameObject[] satamaChaoticObjects;
	public GameObject[] satamaDirtyObjects;
	public GameObject[] satamaCleanObjects;
	public GameObject[] satamaHappyObjects;

	GameObject[] currentKupittaaDegenObjects;
	GameObject[] currentMarttiDegenObjects;
	GameObject[] currentSatamaDegenObjects;
	GameObject[] currentKeskustaDegenObjects;

	public static CityDegenerationManager instance;



	public enum CityAreaName
	{
		Martti = 0,
		Kupittaa = 1,
		Keskusta = 2,
		Satama = 3
	}

	public enum CityAreaCondition
	{
		Happy = 3,
		Clean = 2,
		Dirty = 1,
		Chaotic = 0
	}

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		InitializeAreas();
	}

	public void InitializeAreas()
	{
		cityparts = new CityPart[numberOfCityParts];

		if(PlayerPrefs.HasKey("firstMapIni") == false)
		{
			print("firstTime");
			for(int i = 0; i < numberOfCityParts; i++)
			{
				CityAreaName area = (CityAreaName)i;
				CityAreaCondition condition = CityAreaCondition.Dirty;
				float percentageOfHappyToChaotic = dirtyUpperLimit;

				CityPart part = new CityPart(area, condition, percentageOfHappyToChaotic);
				cityparts[i] = part;

				ActivateDegenObjects(part);
			}

			PlayerPrefs.SetInt("firstMapIni", 1);
			Save();
			PlayerPrefs.Save();
			return;
		}


		for(int i = 0; i < numberOfCityParts; i++)
		{
			CityAreaName area = (CityAreaName)i;
			CityAreaCondition condition;
			float percentageOfHappyToChaotic;

			if(PlayerPrefs.HasKey(cityPartNames[i] + "Condition"))
			{
				
				condition = (CityAreaCondition)PlayerPrefs.GetInt(cityPartNames[i] + "Condition");
			}
			else
			{
				condition = CityAreaCondition.Clean;
			}

			if(PlayerPrefs.HasKey(cityPartNames[i] + "Percentage"))
			{
				percentageOfHappyToChaotic = PlayerPrefs.GetFloat(cityPartNames[i] + "Percentage");
			}
			else
			{
				percentageOfHappyToChaotic = cleanUpperLimit;
			}

			CityPart part = new CityPart(area, condition, percentageOfHappyToChaotic);
			cityparts[i] = part;

			ActivateDegenObjects(part);
		}

		//get the last date when played from playerprefs
		if(PlayerPrefs.HasKey("lastQuitDate"))
		{
			long temp = Convert.ToInt64(PlayerPrefs.GetString("lastQuitDate"));

			//Convert the old time from binary to a DataTime variable
			lastDateWhenQuit = DateTime.FromBinary(temp);

			for(int i = 0; i < numberOfCityParts; i++)
			{
				CompareDateToCurrentDate(cityparts[i]);
			}
		}




	}

	public void CompareDateToCurrentDate(CityPart part)
	{
		CityAreaCondition newConditionOfArea;

		var currentDate = DateTime.Now;
		var dateWhenGameWasQuit = lastDateWhenQuit;

		//TimeSpan totalHoursLeftInTimerWhenGameWasQuit = TimeSpan.FromSeconds(part.timeToNextDegenerationInHours).TotalHours; //secs left when quit

		TimeSpan difference = currentDate.Subtract(dateWhenGameWasQuit); 

		var totalHoursFromWhenLastQuit = difference.TotalHours;

		var percentage = HoursToPercentage(totalHoursFromWhenLastQuit, part.percentageOfHappyToChaotic);

		if(percentage > cleanUpperLimit){ part.condition = CityAreaCondition.Happy; }
		else if(percentage < cleanUpperLimit && percentage > dirtyUpperLimit){ part.condition = CityAreaCondition.Clean; }
		else if(percentage < dirtyUpperLimit && percentage > chaoticUpperLimit){part.condition = CityAreaCondition.Dirty;}
		else if(percentage <= chaoticUpperLimit){part.condition = CityAreaCondition.Chaotic;}

		ChangeCityCondition(part, part.condition);

	}

	public double HoursSinceLastQuitTheGame(DateTime timeWhenLastQuit)
	{
		var currentDate = DateTime.Now;

		TimeSpan difference = currentDate.Subtract(timeWhenLastQuit);

		return difference.TotalHours;
	}

	public float PercentageInHours(float percentage)
	{
		return 20.0f / 100 * percentage;
	}

	public float HoursToPercentage(double totalHoursSinceLastPlayed, float percentageSinceLastQuit)
	{
		var currentHours = 20f / 100 * percentageSinceLastQuit;
		var newHours = currentHours - totalHoursSinceLastPlayed;
		var newPercentage = newHours * 100 / 20f;
		return (float)newPercentage;
	}

	public void ChangeCityCondition(CityPart part, CityAreaCondition condition)
	{
		switch(condition)
		{
			case CityAreaCondition.Happy:
			part.condition = CityAreaCondition.Happy;
			part.percentageOfHappyToChaotic = happyUpperLimit;

			if(part.area == CityAreaName.Kupittaa)
			{
				ActivateDegenObjects(kupittaaHappyObjects, part.area);
			}
			else if(part.area == CityAreaName.Keskusta)
			{
				ActivateDegenObjects(keskustaHappyObjects, part.area);
			}
			else if(part.area == CityAreaName.Martti)
			{
				ActivateDegenObjects(marttiHappyObjects, part.area);
			}
			else if(part.area == CityAreaName.Satama)
			{
				ActivateDegenObjects(satamaHappyObjects, part.area);
			}
			break;

			case CityAreaCondition.Clean:
			part.condition = CityAreaCondition.Clean;
			part.percentageOfHappyToChaotic = cleanUpperLimit;

			if(part.area == CityAreaName.Kupittaa)
			{
				ActivateDegenObjects(kupittaaCleanObjects, part.area);
			}
			else if(part.area == CityAreaName.Keskusta)
			{
				ActivateDegenObjects(keskustaCleanObjects, part.area);
			}
			else if(part.area == CityAreaName.Martti)
			{
				ActivateDegenObjects(marttiCleanObjects, part.area);
			}
			else if(part.area == CityAreaName.Satama)
			{
				ActivateDegenObjects(satamaCleanObjects, part.area);
			}
			break;

			case CityAreaCondition.Dirty:
			part.condition = CityAreaCondition.Dirty;
			part.percentageOfHappyToChaotic = dirtyUpperLimit;

			if(part.area == CityAreaName.Kupittaa)
			{
				ActivateDegenObjects(kupittaaDirtyObjects, part.area);
			}
			else if(part.area == CityAreaName.Keskusta)
			{
				ActivateDegenObjects(keskustaDirtyObjects, part.area);
			}
			else if(part.area == CityAreaName.Martti)
			{
				ActivateDegenObjects(marttiDirtyObjects, part.area);
			}
			else if(part.area == CityAreaName.Satama)
			{
				ActivateDegenObjects(satamaDirtyObjects, part.area);
			}
			break;

			case CityAreaCondition.Chaotic:
			part.condition = CityAreaCondition.Chaotic;
			part.percentageOfHappyToChaotic = chaoticUpperLimit;

			if(part.area == CityAreaName.Kupittaa)
			{
				ActivateDegenObjects(kupittaaChaoticObjects, part.area);
			}
			else if(part.area == CityAreaName.Keskusta)
			{	
				ActivateDegenObjects(keskustaChaoticObjects, part.area);
			}
			else if(part.area == CityAreaName.Martti)
			{
				ActivateDegenObjects(marttiChaoticObjects, part.area);
			}
			else if(part.area == CityAreaName.Satama)
			{
				ActivateDegenObjects(satamaChaoticObjects, part.area);
			}
			break;
		}

		Save();

	}


	void ActivateDegenObjects(GameObject[] objectsToActivate, CityAreaName currentObjectsToDeactivate)
	{
		switch(currentObjectsToDeactivate)
		{
			case CityAreaName.Kupittaa:
			DeactivateObjects(currentKupittaaDegenObjects);
			foreach(GameObject go in objectsToActivate)
			{
				go.SetActive(true);
			}
			currentKupittaaDegenObjects = objectsToActivate;
			break;

			case CityAreaName.Keskusta:
			DeactivateObjects(currentKeskustaDegenObjects);
			foreach(GameObject go in objectsToActivate)
			{
				go.SetActive(true);
			}
			currentKeskustaDegenObjects = objectsToActivate;
			break;

			case CityAreaName.Martti:
			DeactivateObjects(currentMarttiDegenObjects);
			foreach(GameObject go in objectsToActivate)
			{
				go.SetActive(true);
			}
			currentMarttiDegenObjects = objectsToActivate;
			break;

			case CityAreaName.Satama:
			DeactivateObjects(currentSatamaDegenObjects);
			foreach(GameObject go in objectsToActivate)
			{
				go.SetActive(true);
			}
			currentSatamaDegenObjects = objectsToActivate;
			break;
		}




	}

	void ActivateDegenObjects(CityPart part)
	{
		switch(part.area)
		{
			case CityAreaName.Kupittaa:
			if(part.condition == CityAreaCondition.Happy)
			{
				ActivateDegenObjects(kupittaaHappyObjects, CityAreaName.Kupittaa);
				kupittaaBlackness.color = new Color(0,0,0, happyDeGenAlpha);
			} 
			if(part.condition == CityAreaCondition.Clean)
			{
				ActivateDegenObjects(kupittaaCleanObjects, CityAreaName.Kupittaa);
				kupittaaBlackness.color = new Color(0,0,0, cleanDeGenAlpha);
			} 
			if(part.condition == CityAreaCondition.Dirty)
			{
				ActivateDegenObjects(kupittaaDirtyObjects, CityAreaName.Kupittaa);
				kupittaaBlackness.color = new Color(0,0,0, dirtyDeGenAlpha);
			} 
			if(part.condition == CityAreaCondition.Chaotic)
			{
				ActivateDegenObjects(kupittaaChaoticObjects, CityAreaName.Kupittaa);
				kupittaaBlackness.color = new Color(0,0,0, chaoticDeGenAlpha);
			} 
			break;

			case CityAreaName.Martti:
			if(part.condition == CityAreaCondition.Happy)
			{
				ActivateDegenObjects(marttiHappyObjects, CityAreaName.Martti);
				marttiBlackness.color = new Color(0,0,0, happyDeGenAlpha);
			} 
			if(part.condition == CityAreaCondition.Clean)
			{
				ActivateDegenObjects(marttiCleanObjects, CityAreaName.Martti);
				marttiBlackness.color = new Color(0,0,0, cleanDeGenAlpha);
			} 
			if(part.condition == CityAreaCondition.Dirty)
			{
				ActivateDegenObjects(marttiDirtyObjects, CityAreaName.Martti);
				marttiBlackness.color = new Color(0,0,0, dirtyDeGenAlpha);
			} 
			if(part.condition == CityAreaCondition.Chaotic)
			{
				ActivateDegenObjects(marttiChaoticObjects, CityAreaName.Martti);
				marttiBlackness.color = new Color(0,0,0, chaoticDeGenAlpha);
			} 
			break;

			case CityAreaName.Satama:
			if(part.condition == CityAreaCondition.Happy)
			{
				ActivateDegenObjects(satamaHappyObjects, CityAreaName.Satama);
				satamaBlackness.color = new Color(0,0,0, happyDeGenAlpha);
			} 
			if(part.condition == CityAreaCondition.Clean)
			{
				ActivateDegenObjects(satamaCleanObjects, CityAreaName.Satama);
				satamaBlackness.color = new Color(0,0,0, cleanDeGenAlpha);
			} 
			if(part.condition == CityAreaCondition.Dirty)
			{
				ActivateDegenObjects(satamaDirtyObjects, CityAreaName.Satama);
				satamaBlackness.color = new Color(0,0,0, dirtyDeGenAlpha);
			} 
			if(part.condition == CityAreaCondition.Chaotic)
			{
				ActivateDegenObjects(satamaChaoticObjects, CityAreaName.Satama);
				satamaBlackness.color = new Color(0,0,0, chaoticDeGenAlpha);
			} 
			break;

			case CityAreaName.Keskusta:
			if(part.condition == CityAreaCondition.Happy)
			{
				ActivateDegenObjects(keskustaHappyObjects, CityAreaName.Keskusta);
				keskustaBlackness.color = new Color(0,0,0, happyDeGenAlpha);
			} 
			if(part.condition == CityAreaCondition.Clean)
			{
				ActivateDegenObjects(keskustaCleanObjects, CityAreaName.Keskusta);
				keskustaBlackness.color = new Color(0,0,0, cleanDeGenAlpha);
			} 
			if(part.condition == CityAreaCondition.Dirty)
			{
				ActivateDegenObjects(keskustaDirtyObjects, CityAreaName.Keskusta);
				keskustaBlackness.color = new Color(0,0,0, dirtyDeGenAlpha);
			} 
			if(part.condition == CityAreaCondition.Chaotic)
			{
				ActivateDegenObjects(keskustaChaoticObjects, CityAreaName.Keskusta);
				keskustaBlackness.color = new Color(0,0,0, chaoticDeGenAlpha);
			} 
			break;



		}
	}

	void ActivateDegenObjects(CityAreaCondition condition)
	{
		
	}

	void DeactivateObjects(GameObject[] objects)
	{
		if(objects != null)
		{
			foreach(GameObject ob in objects)
			{
				ob.SetActive(false);
			}
		}

	}

	public void Save()
	{
		for(int i = 0; i < numberOfCityParts; i++)
		{
			PlayerPrefs.SetInt(cityPartNames[i] + "Condition", (int)cityparts[i].condition);
			PlayerPrefs.SetFloat(cityPartNames[i] + "Percentage", cityparts[i].percentageOfHappyToChaotic);

			PlayerPrefs.SetString("lastQuitDate", DateTime.Now.ToBinary().ToString());
		}
	}

	void OnApplicationQuit()
	{
		Save();
	}
}

public class CityPart
{
	public CityDegeneration.CityAreaName area {get; set;}
	public CityDegeneration.CityAreaCondition condition {get; set;}
	public DateTime timeWhenLastQuit {get; set;}
	public float percentageOfHappyToChaotic {get; set;}

	public CityPart(CityDegeneration.CityAreaName area, CityDegeneration.CityAreaCondition condition, float percentageOfHappyToChaotic)
	{
		this.area = area;
		this.condition = condition;
		this.percentageOfHappyToChaotic = percentageOfHappyToChaotic;
	}

	public CityPart(){}
}
	

	
