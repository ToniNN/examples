﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {

	//public variables for debugging and dependencies in Editor
	public static SoundManager instance;
	public AudioSource[] soundsources;
	public AudioClip[] effects;
	public bool mainMusicOn;
	public AudioSource miniGameMusic;
	public bool miniGameMusicPlaying;

	public enum Sounds
	{
		MiniGameWin = 0,
		MiniGameLost = 1,
		BuyBuilding = 2,
		Transition = 3,
		MoneyEarned = 4,
		IcePickHit = 5,
		IcePickDestroyed = 6,
		FenceFixed = 7,
		JennaJumps = 8,
		JennaFallsDown = 9,
		LetterRightPlaceOrFlowerPlanted = 10,
		RightTrashOrTrashSorting = 11,
		PipeFixed = 12,
		Swoosh = 13,
		HoleFilling = 14,
		LeafCollected = 15,
		WalkingGameStep = 16,
		JennaFallsDownWalkingGame = 17,
		WindowBreaking = 18,
		CloseShopSound = 19
	}

	void Start() 
	{
		if (instance == null) 
		{
			DontDestroyOnLoad(gameObject);
			instance = this;
		}
		else if (instance != this) 
			Destroy(gameObject);

		soundsources = GetComponents<AudioSource>();
	}



	public void PlaySound(Sounds audio) 
	{
			foreach (AudioSource au in soundsources)
			{
				if (!au.isPlaying)
				{
					au.clip = effects[(int)audio];
					au.pitch = 1.0f;
					au.Play();
					break;
				}
			}
		
	   
	   
	}

	public void PlaySoundWithCustomPitch(Sounds audio, float pitchLevel) 
	{
			foreach (AudioSource au in soundsources)
			{
				if (!au.isPlaying)
				{
					au.clip = effects[(int)audio];
					au.pitch = pitchLevel;
					au.Play();
					break;
				}
			}
		
	   
	   
	}

	public void StartMiniGameMusic()
	{
		miniGameMusic.Play();
		miniGameMusicPlaying = true;
	}

	public void StopMiniGameMusic()
	{
		miniGameMusic.Stop();
		miniGameMusicPlaying = false;
	}




}
