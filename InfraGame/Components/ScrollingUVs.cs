﻿using UnityEngine;
using System.Collections;
 
public class ScrollingUVs : MonoBehaviour 
{
	//public variables for debugging and dependencies in Editor
	public int materialIndex = 0;
	public Vector2 uvAnimationRate = new Vector2( 1.0f, 0.0f );
	public string textureName = "_MainTex";

	private bool m_scrolling = true;
	private Vector2 m_uvOffset = Vector2.zero;
 
	void LateUpdate() 
	{
		if (!m_scrolling)
			return;
		m_uvOffset += ( uvAnimationRate * Time.deltaTime );
		if( GetComponent<Renderer>().enabled )
		{
			GetComponent<Renderer>().materials[ materialIndex ].SetTextureOffset( textureName, m_uvOffset );
		}
	}

	public void StopScrolling(){
		m_scrolling = false;
	}
}