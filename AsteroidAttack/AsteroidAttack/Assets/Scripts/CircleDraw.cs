﻿using UnityEngine;

/// <summary>
/// Draws a circle with a defined radius with a LineRendererComponent
/// </summary>
[RequireComponent(typeof(LineRenderer))]
public class CircleDraw : MonoBehaviour
{
	private float radius;
	private float theta_scale = 0.01f;        
	private int size;
	private LineRenderer lineRenderer;

	void Awake()
	{
		float sizeValue = (2.0f * Mathf.PI) / theta_scale;
		size = (int)sizeValue;
		size++;
		lineRenderer = GetComponent<LineRenderer>();
		lineRenderer.startWidth = 0.1f;
		lineRenderer.endWidth = 0.1f;
		lineRenderer.positionCount = size;
	}

	void Start()
	{
		//get the radius from the DefenseSystem.cs
		radius = DefenseSystem.instance.safetyZoneRadius;
	}

	void Update()
	{
		Vector3 pos;
		float theta = 0f;

		for (int i = 0; i < size; i++)
		{
			theta += (2.0f * Mathf.PI * theta_scale);
			float x = radius * Mathf.Cos(theta);
			float y = radius * Mathf.Sin(theta);
			x += gameObject.transform.position.x;
			y += gameObject.transform.position.y;
			pos = new Vector3(x, y, 0);
			lineRenderer.SetPosition(i, pos);
		}
	}
}