﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class WordMiniGame : MonoBehaviour {

	//public variables for debugging and dependencies in Editor
	public Image timerImage;
	public RoundTimer timer;
	public List<GameObject> words = new List<GameObject>();
	
	private GameObject m_currentlySelectedObject;
	private GameObject m_currentParent;
	private Vector3 m_offset;
	private Vector2 m_hitPoint;
	private BoxCollider2D m_letterSpace;
	private int m_lettersPlaced = 0;
	private List<GameObject> m_letterSpaces = new List<GameObject>();
	private bool m_isCompleted;
	private Vector3 m_newPos;

	void OnEnable()
	{
		timer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		timer.OnTimerEnd -= RoundEnd;
	}

	void Awake ()
	{
		Initialization ();
	}

	public void Initialization()
	{
		timer = GetComponent<RoundTimer>();
		timer.timerImage = timerImage;
		StartCoroutine(timer.StartTimer(GameManager.instance.currentRoundDuration));
		var index = Random.Range (0, words.Count);
		Instantiate (words [index]);
	}

	void Update()
	{
		CheckForInput();
	}

	void CheckForInput()
	{
		if (Input.GetButton ("Fire1")) 
		{	
			if(m_currentlySelectedObject == null)
			{
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit2D hit;
				hit = Physics2D.Raycast (ray.origin, ray.direction);

				if(hit.collider != null)
				{
					if(hit.collider.tag == "Letter" && m_currentlySelectedObject == null)
					{
						m_currentlySelectedObject = hit.collider.gameObject;
						m_currentParent = m_currentlySelectedObject.transform.parent.gameObject; //currently selected object's parent :D
						m_hitPoint = hit.point;
						m_offset.x = hit.point.x - m_currentlySelectedObject.transform.position.x;
						m_offset.y = hit.point.y - m_currentlySelectedObject.transform.position.y;
					}
				}


			}

			if(m_currentlySelectedObject != null)
			{
				Drag();
			}

		}
		else if(Input.GetButtonUp("Fire1") && m_currentlySelectedObject != null)
		{
			CheckIfPlanted();
		}
	}

	void Drag()
	{
		Vector3 touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		Vector3 newPosition = new Vector3(touchPosition.x - m_offset.x, touchPosition.y - m_offset.y, 0);
		m_currentlySelectedObject.transform.position = newPosition;
	}

	void CheckIfPlanted()
	{
		m_letterSpace = m_currentParent.GetComponent<BoxCollider2D> ();
		if (m_currentlySelectedObject.GetComponent<BoxCollider2D> ().IsTouching (m_letterSpace)) 
		{
			m_currentlySelectedObject.GetComponent<BoxCollider2D>().enabled = false;
			m_currentlySelectedObject.transform.localPosition = new Vector3(0f, -0.5f, 0f);
			m_letterSpace.enabled = false;
			m_currentlySelectedObject = null;
			m_lettersPlaced++;
			SoundManager.instance.PlaySound(SoundManager.Sounds.LetterRightPlaceOrFlowerPlanted);

			if (m_lettersPlaced == m_letterSpaces.Count && !m_isCompleted) {
				GameManager.instance.RoundEndActions (true);
				m_isCompleted = true;
			}
		}
		else if (CheckIfCollidesLetterSpaces() && !m_isCompleted) 
		{
			GameManager.instance.RoundEndActions (false);
			m_isCompleted = true;
		}
		else
		{
			m_currentlySelectedObject = null;
		}
	}
	public void AddLetterSpace(GameObject lettSpa)
	{
		m_letterSpaces.Add (lettSpa);
	}

	bool CheckIfCollidesLetterSpaces() 
	{
		foreach (GameObject lettSpa in m_letterSpaces)
		{
			if (lettSpa.GetComponent<BoxCollider2D> () != null) 
			{
				if (m_currentlySelectedObject.GetComponent<BoxCollider2D> ().IsTouching (lettSpa.GetComponent<BoxCollider2D> ())) 
				{
					return true;
				}
			}
		}
		return false;
	}

	public void RoundEnd()
	{
		if (!m_isCompleted) {
			m_isCompleted = true;
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	public bool IsCompleted() 
	{
		return false;
	}

}
