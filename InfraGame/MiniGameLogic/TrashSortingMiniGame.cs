﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TrashSortingMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public Image timerImage;
	public RoundTimer timer;
	public Sprite[] trashSprites;
	public GameObject[] trashCans;

	private GameObject m_currentlySelectedObject;
	private Vector3 m_offset;
	private Vector2 m_hitPoint;
	private Vector3 m_newPos;
	private bool m_completed;
	private int m_rand;
	private Collider2D m_rightTrashCan;

	void OnEnable()
	{
		timer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		timer.OnTimerEnd -= RoundEnd;
	}

	void Awake ()
	{
		Initialization ();
	}

	public void Initialization()
	{
		timer = GetComponent<RoundTimer>();
		timer.timerImage = timerImage;
		StartCoroutine(timer.StartTimer(GameManager.instance.currentRoundDuration));

		m_rand = Random.Range (0, 5);
		GetComponent<SpriteRenderer> ().sprite = trashSprites [m_rand];
		m_rightTrashCan = trashCans [m_rand].GetComponent<BoxCollider2D>();
	}

	void Update()
	{
		CheckForInput();
	}

	void CheckForInput()
	{
		if (Input.GetButton ("Fire1")) 
		{	
			if(m_currentlySelectedObject == null)
			{
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit2D hit;
				hit = Physics2D.Raycast (ray.origin, ray.direction);

				if(hit.collider != null)
				{
					if(hit.collider.tag == "Trash" && m_currentlySelectedObject == null)
					{
						m_currentlySelectedObject = hit.collider.gameObject;
						m_hitPoint = hit.point;
						m_offset.x = hit.point.x - m_currentlySelectedObject.transform.position.x;
						m_offset.y = hit.point.y - m_currentlySelectedObject.transform.position.y;
					}
				}
			}
			if(m_currentlySelectedObject != null)
			{
				Drag();
			}
		}
		else if(Input.GetButtonUp("Fire1") && m_currentlySelectedObject != null)
		{
			CheckIfRightCan();
		}
	}

	void Drag()
	{
		Vector3 touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		Vector3 newPosition = new Vector3(touchPosition.x - m_offset.x, touchPosition.y - m_offset.y, 0);
		m_currentlySelectedObject.transform.position = newPosition;
	}

	void CheckIfRightCan()
	{
		if (m_currentlySelectedObject.GetComponent<BoxCollider2D> ().IsTouching (m_rightTrashCan) && !m_completed) {
			m_currentlySelectedObject.GetComponent<SpriteRenderer> ().enabled = false;	//sprite piiloon
			m_currentlySelectedObject.gameObject.GetComponent<BoxCollider2D> ().enabled = false;	//collider piiloon
			m_currentlySelectedObject = null;
			m_completed = true;
			GameManager.instance.RoundEndActions (true);

			SoundManager.instance.PlaySound(SoundManager.Sounds.RightTrashOrTrashSorting);

		} else if (Physics2D.OverlapCircleAll (transform.position, 1f).Length > 1 && !m_completed) {
			m_completed =true;
			GameManager.instance.RoundEndActions (false);
		}
		else
		{
			m_currentlySelectedObject = null;
		}
	}

	public void RoundEnd()
	{
		if (!m_completed) {
			m_completed = true;
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	public bool IsCompleted() 
	{
		return false;
	}

}
