﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using TMPro;


public class GameManager : MonoBehaviour {

	//Singleton instance
	public static GameManager instance;

	//Round Objects
	[Header("Round Objects")]
	public RoundTimer.RoundDuration currentRoundDuration;
	public Round currentRound;

	//Locations
	[Header("Locations")]
	public MiniGameLocations currentLocation;
	
	//Score
	[Header("Scoreboard")]
	public ScoreBoard scoreBoard;

	//Getters & setters, members
	private int m_points;
	[HideInInspector]
	public int points { get { return m_points; } set { m_points = value; } }

	private bool m_lifeLost;
	[HideInInspector]
	public bool lifeLost { get { return m_lifeLost; } set { m_lifeLost = value; } }

	private int m_lives;
	[HideInInspector]
	public int lives { get { return m_lives; } set { m_lives = value; } }

	private int  m_moneyEarnedDuringCityRound;
	[HideInInspector]
	public int moneyEarnedDuringCityRound { get { return m_moneyEarnedDuringCityRound; } set { m_moneyEarnedDuringCityRound = value; }  }

	private bool m_faster;
	private List<int> m_currentMiniGameOrder;
	private int m_currentIndexInList;
	private bool m_showRoundEndedScreen;
	private bool m_conditionChanged;

	//Sprites
	[Header("Sprite References")]
	public Sprite lifeSprite, loseLifeSprite, lifeBackgroundSprite, loseLifeBackgroundSprite;

	//Tutorial
	[Header("Tutorial Objects")]
	public GameObject tutorialWindow;
	public TextMeshProUGUI tutorialText;

	//For debugging in the Editor
	[Header("Debug")]
	public bool debugLevelOrder;
	public int[] debugLevelOrderArray;

	//References needed in the hubworld scene
	public MapReferences mapReferences;

	//Object used to show high five animation
	public GameObject highFiveObject;
	
	//Analytics object
	public GoogleAnalyticsV4 googleAnalyticsPrefab;
	
	//Enums
	public enum Round
	{
		First = 1,
		Second = 2,
		Third = 3,
		Fourth = 4
	}

	public enum MiniGameLocations
	{
		Kupittaa = 1,
		Martti = 0,
		Satama = 3,
		Keskusta = 2
	}

	public enum MarttiMiniGames
	{
		GraffitiCleaning = 1,
		RakingGame = 2,
		FenceMiniGame = 3,
		BalancingMiniGame = 4,
		WindowGame = 5
	}

	public enum KupittaaMiniGames
	{
		TrashPicking = 6,
		SandRoad = 14,
		Trimming = 15,
		TrashSorting = 18,
		MowingLawnGame = 20
	}

	public enum SatamaMiniGames
	{
		SnowShovelingGame = 7,
		CarDrivingGame = 8,
		PipeMiniGame = 9,
		HoleFillingGame = 10,
		WordGame = 16
	}

	public enum KeskustaMiniGames
	{
		IceClearingGame = 11,
		ToolPickingGame = 12,
		FlowerPlantingGame = 13,
		WalkingGame = 17,	
		ObstacleJumpingGame = 19
	}

	//called when the game starts
	void Awake()
	{
		//Singleton init
		if(instance != null)
			Destroy(this.gameObject);
		else
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
		}

		LoadPlayerData();

		m_currentMiniGameOrder = new List<int>();

		mapReferences = GameObject.Find("MapReferences").GetComponent<MapReferences>();

		//should tutorial be started
		if(PlayerPrefs.HasKey("firstStart") == false)
		{
			StartCoroutine(CloudsAndTutorialHeads());
		}
		else
			mapReferences.tutorialCanvas.SetActive(false);
	}

	void LoadPlayerData()
	{
		if (PlayerPrefs.HasKey("Money"))
		{
			points = PlayerPrefs.GetInt("Money");
		}
	}

	//Tutorial methods

	IEnumerator	CloudsAndTutorialHeads()
	{
		mapReferences.tutorialCanvas.SetActive(true);
		PlayerPrefs.SetInt("firstStart", 1);
		PlayerPrefs.Save();
		yield return new WaitForSeconds(1f);
		mapReferences.ilmariAndJenna.SetActive(true);
		yield return new WaitForSeconds(0.5f);
		mapReferences.firstTutorialBubble.SetActive(true);
	}

	public void TutorialStep2() //called from a UI-button, dependency set in Editor
	{
		mapReferences.firstTutorialBubble.SetActive(false);
		mapReferences.secondTutorialBubble.SetActive(true);
		mapReferences.tutorialCanvasGroup.blocksRaycasts = false;
		mapReferences.keskustaGlowAnimator.Play("keskustaGlow", -1, 0);
	}

	//Minigame starter methods
	public void StartNextMiniGameInList()
	{
		if(m_currentIndexInList < m_currentMiniGameOrder.Count - 1)
		{
			m_currentIndexInList++;
			SceneManager.LoadScene(m_currentMiniGameOrder[m_currentIndexInList]);
		}
	}

	public IEnumerator StartNextMiniGameInListWithDelay()
	{
		yield return new WaitForSeconds(1.5f);
		StartNextMiniGameInList();
	}

	IEnumerator StartSceneByIndexDelay(int sceneToLoad)
	{
		yield return new WaitForSeconds(2.0f);
		SceneManager.LoadScene(sceneToLoad);
	}

	//Transition methods
	public void KupittaaTransition()
	{
		mapReferences.kupittaaTransition.gameObject.SetActive(true);
		SoundManager.instance.PlaySound(SoundManager.Sounds.Transition);
		GameManager.instance.googleAnalyticsPrefab.LogScreen("KupittaaSiirtymä");
	}

	public void MarttiTransition()
	{
		mapReferences.marttiTransition.gameObject.SetActive(true);
		SoundManager.instance.PlaySound(SoundManager.Sounds.Transition);
		GameManager.instance.googleAnalyticsPrefab.LogScreen("MarttiSiirtymä");
	}

	public void KeskustaTransition()
	{
		//for tutorial
		if(PlayerPrefs.HasKey("firstTimePressKeskusta") == false)
		{
			mapReferences.ilmariAndJenna.SetActive(false);
			mapReferences.secondTutorialBubble.SetActive(false);
			PlayerPrefs.SetInt("firstTimePressKeskusta", 1);
			PlayerPrefs.Save();
		}

		mapReferences.keskustaTransition.gameObject.SetActive(true);
		SoundManager.instance.PlaySound(SoundManager.Sounds.Transition);
		GameManager.instance.googleAnalyticsPrefab.LogScreen("KeskustaSiirtymä");
	}

	public void SatamaTransition()
	{
		mapReferences.satamaTransition.gameObject.SetActive(true);
		SoundManager.instance.PlaySound(SoundManager.Sounds.Transition);
		GameManager.instance.googleAnalyticsPrefab.LogScreen("SatamaSiirtymä");
	}

	//Methods for UI-Buttons, method for each city part because Unity UI-buttons onClick-events cant take methods with needed arguments in the Editor, usability for designers
	public void StartKupittaa()
	{
		SoundManager.instance.PlaySound(SoundManager.Sounds.Transition);
		scoreBoard.ResetScoreBoard();
		m_currentMiniGameOrder = new List<int>{6,14,15,18,20};

		Shuffle(m_currentMiniGameOrder);

		if(debugLevelOrder)
		{
			List<int> customOrder = new List<int>(debugLevelOrderArray);
			m_currentMiniGameOrder = customOrder;
		}

		lives = 3;
		currentRoundDuration = RoundTimer.RoundDuration.Twenty;
		currentRound = Round.First;
		m_currentIndexInList = 0;
		currentLocation = MiniGameLocations.Kupittaa;
		moneyEarnedDuringCityRound = 0;

		GameManager.instance.googleAnalyticsPrefab.LogScreen("KupittaaPelit");
		SceneManager.LoadScene(m_currentMiniGameOrder[m_currentIndexInList], LoadSceneMode.Single);
	}

	public void StartMartti()
	{
		SoundManager.instance.PlaySound(SoundManager.Sounds.Transition);
		scoreBoard.ResetScoreBoard();
		m_currentMiniGameOrder = new List<int>{1,2,3,4,5};

		Shuffle(m_currentMiniGameOrder);

		if(debugLevelOrder)
		{
			List<int> customOrder = new List<int>(debugLevelOrderArray);
			m_currentMiniGameOrder = customOrder;
		}

		lives = 3;
		currentRoundDuration = RoundTimer.RoundDuration.Twenty;
		currentRound = Round.First;
		m_currentIndexInList = 0;
		currentLocation = MiniGameLocations.Martti;
		moneyEarnedDuringCityRound = 0;

		GameManager.instance.googleAnalyticsPrefab.LogScreen("MarttiPelit");
		SceneManager.LoadScene(m_currentMiniGameOrder[m_currentIndexInList]); 
	}

	public void StartKeskusta()
	{
		SoundManager.instance.PlaySound(SoundManager.Sounds.Transition);
		scoreBoard.ResetScoreBoard();
		m_currentMiniGameOrder = new List<int>{11,12,13,17,19};

		Shuffle(m_currentMiniGameOrder);

		if(debugLevelOrder)
		{
			List<int> customOrder = new List<int>(debugLevelOrderArray);
			m_currentMiniGameOrder = customOrder;
		}

		lives = 3;
		currentRoundDuration = RoundTimer.RoundDuration.Twenty;
		currentRound = Round.First;
		m_currentIndexInList = 0;
		currentLocation = MiniGameLocations.Keskusta;
		moneyEarnedDuringCityRound = 0;

		GameManager.instance.googleAnalyticsPrefab.LogScreen("KeskustaPelit");
		SceneManager.LoadScene(m_currentMiniGameOrder[m_currentIndexInList]); 

	}

	public void StartSatama()
	{
		SoundManager.instance.PlaySound(SoundManager.Sounds.Transition);

		scoreBoard.ResetScoreBoard();
		m_currentMiniGameOrder = new List<int>{7,8,9,10,16};

		Shuffle(m_currentMiniGameOrder);

		if(debugLevelOrder)
		{
			List<int> customOrder = new List<int>(debugLevelOrderArray);
			m_currentMiniGameOrder = customOrder;
		}

		lives = 3;
		currentRoundDuration = RoundTimer.RoundDuration.Twenty;
		currentRound = Round.First;
		m_currentIndexInList = 0;
		currentLocation = MiniGameLocations.Satama;
		moneyEarnedDuringCityRound = 0;

		GameManager.instance.googleAnalyticsPrefab.LogScreen("KeskustaPelit");
		SceneManager.LoadScene(m_currentMiniGameOrder[m_currentIndexInList]);

	}

	public IEnumerator StartLocationContinued(Round round)
	{
		m_faster = true;
		yield return new WaitForSeconds(1.5f);

		m_currentIndexInList = 0;

		int[] values = new int[5];

		switch(currentLocation)
		{
			case MiniGameLocations.Kupittaa:
			values = new int[]{6,14,15,18,20};
			break;

			case MiniGameLocations.Keskusta:
			values = new int[]{11,12,13,17,19};
			break;

			case MiniGameLocations.Satama:
			values = new int[]{7,8,9,10,16};
			break;

			case MiniGameLocations.Martti:
			values = new int[]{1,2,3,4,5};
			break;
		}

		if(values != null)
			m_currentMiniGameOrder = new List<int>(values);

		Shuffle(m_currentMiniGameOrder);

		currentRound = round;

		switch(currentRound)
		{
			case Round.Second:
			currentRoundDuration = RoundTimer.RoundDuration.Fifteen;
			break;

			case Round.Third:
			currentRoundDuration = RoundTimer.RoundDuration.Ten;
			break;

			case Round.Fourth:
			currentRoundDuration = RoundTimer.RoundDuration.Five;
			break;
		}

		GameManager.instance.googleAnalyticsPrefab.LogEvent("Minipelit", "UusiMinipeliKierros", string.Format("Kohde: {0}", currentLocation.ToString()), 1);
		SceneManager.LoadScene(m_currentMiniGameOrder[m_currentIndexInList]);


	}

	private void Shuffle(List<int> list)
	{
		int n = list.Count;
		while (n > 1) 
		{
			int k = (Random.Range(0, n) % n);
			n--;
			int value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	IEnumerator ShowHighFive()
	{
		highFiveObject.SetActive(true);
		yield return new WaitForSeconds(1.5f);
		highFiveObject.SetActive(false);
	}

	//Called at the end of each round
	public void RoundEndActions(bool completedMinigame)
	{
		var timer = GameObject.FindObjectOfType<RoundTimer>();
		timer.Stop();

		if(completedMinigame)
		{
			SoundManager.instance.PlaySound(SoundManager.Sounds.MiniGameWin);
			GetScoreFromMiniGame();
			ShowScoreBoard();
			StartCoroutine(ShowHighFive());
			
		}
		else
		{
			SoundManager.instance.PlaySound(SoundManager.Sounds.MiniGameLost);
			LoseALife();
			ShowScoreBoard();
			highFiveObject.SetActive(false);
		}

		//check if we have reached the end of the minigames list and we are not dead
		if(m_currentIndexInList >= m_currentMiniGameOrder.Count - 1 && lives > 0)
		{
			Round nextRound = (int)currentRound < 4 ? (Round)((int)currentRound + 1) : Round.Fourth;
			print(nextRound);
			StartCoroutine(StartLocationContinued(nextRound));
		}
		else if(lives == 0)
		{
			print("dead, go back to map");
			points += moneyEarnedDuringCityRound;
			PlayerPrefs.SetInt("Money", points);
			m_showRoundEndedScreen = true;
			StartCoroutine(StartSceneByIndexDelay(0));

			m_conditionChanged = true;
		}
		else
		{
			StartCoroutine(StartNextMiniGameInListWithDelay());	
		}

		
	}


	void IncreaseCityPartConditionLevel(MiniGameLocations location)
	{
		int conditionToIncreaseTo = (int)CityDegeneration.instance.cityparts[(int)location].condition + 1;

		if(conditionToIncreaseTo > 3) 
			return;

		CityDegeneration.instance.ChangeCityCondition(CityDegeneration.instance.cityparts[(int)location], (CityDegeneration.CityAreaCondition)conditionToIncreaseTo);
	}

	//delayed version for design purposes
	IEnumerator IncreaseCityPartConditionWithDelay()
	{
		yield return new WaitForSeconds(0.2f);
		IncreaseCityPartConditionLevel(currentLocation);
		m_conditionChanged = false;
	}

	void GetScoreFromMiniGame()
	{
		switch(currentRound)
		{
			case Round.First:
			moneyEarnedDuringCityRound += 100;
			break;
			case Round.Second:
			moneyEarnedDuringCityRound += 200;
			break;
			case Round.Third:
			moneyEarnedDuringCityRound += 300;
			break;
			case Round.Fourth:
			moneyEarnedDuringCityRound += 400;
			break;	
		}
	}

	void StartNewMiniGamesRound(MiniGameLocations location)
	{
		switch(location)
		{
			case MiniGameLocations.Kupittaa:
			StartKupittaa();
			break;
		}
	}

	public void ShowScoreBoard()
	{
		tutorialText.enabled = false; 
		scoreBoard.ShowBoard();
	}

	public void DisableScoreBoard()
	{
		tutorialText.enabled = true; 
		scoreBoard.scoreBoard.SetActive(false);
	}

	public void LoseALife()
	{
		lives--;
		lifeLost = true;
	}

	//called when the app closes
	void OnApplicationQuit()
	{
		PlayerPrefs.SetInt("Money", points);
		PlayerPrefs.Save();
	}


	void OnLevelWasLoaded(int level)
	{
		//we have loaded a minigame
		if(level > 0)
		{
			if(!SoundManager.instance.miniGameMusicPlaying)
				SoundManager.instance.StartMiniGameMusic();

			if(m_faster)
			{
				if(scoreBoard.gameObject.activeSelf)
				{
					scoreBoard.ShowFasterWrapper();
					m_faster = false;
				}
				else
					m_faster = false;
			}
		}

		//we have loaded the map, not a minigame
		else if(level == 0)
		{	
			SoundManager.instance.StopMiniGameMusic();

			GameManager.instance.googleAnalyticsPrefab.LogScreen("KarttaNäkymä");

			if (m_showRoundEndedScreen)
			{
				if(GameManager.instance.mapReferences == null)
					GameManager.instance.mapReferences = GameObject.Find("MapReferences").GetComponent<MapReferences>();

				m_showRoundEndedScreen = false;
				mapReferences.roundsEndedScreen.SetActive(true);
				mapReferences.roundsEndedScore.text = moneyEarnedDuringCityRound.ToString();

				SoundManager.instance.PlaySound(SoundManager.Sounds.MoneyEarned);
			}

			if(m_conditionChanged)
			{
				StartCoroutine(IncreaseCityPartConditionWithDelay());
			}

			//tutorial
			if(PlayerPrefs.HasKey("firstTimeFromKeskustaMinigamesToMap") == false)
			{
				mapReferences.tutorialCanvas.SetActive(true);
				mapReferences.tutorialClouds.SetActive(false);
				StartCoroutine(FirstTimeBackToMapTutorialWithDelay());
			}

			if(PlayerPrefs.HasKey("firstTimeFromKeskustaMinigamesToMap") && PlayerPrefs.HasKey("secondTimeFromMiniGamesToMap") == false)
			{
				mapReferences.tutorialCanvas.SetActive(true);
				mapReferences.tutorialClouds.SetActive(false);
				mapReferences.secondTutorialBubble.SetActive(true);
				mapReferences.tutorialSecondBubbleButton.gameObject.SetActive(true);
				var text = mapReferences.secondTutorialBubble.GetComponentInChildren<TextMeshProUGUI>();
				text.text = "Kaupunkia hoitamalla säästät rahaa. <color=yellow>Kaupasta (oikea yläkulma)</color> voit tehdä kaupungille hienoja hankintoja säästämälläsi rahalla!";
				mapReferences.ilmariAndJenna.SetActive(true);

				PlayerPrefs.SetInt("secondTimeFromMiniGamesToMap", 1);
				PlayerPrefs.Save();

				mapReferences.shopButtonAnimator.Play("pulse", -1, 0);
			}
		}

		DisableScoreBoard();
	}

	IEnumerator FirstTimeBackToMapTutorialWithDelay()
	{
		yield return new WaitForSeconds(0.5f);
		mapReferences.ilmariAndJenna.SetActive(true);
		mapReferences.thirdTutorialBubble.SetActive(true);
		PlayerPrefs.SetInt("firstTimeFromKeskustaMinigamesToMap", 1);
		PlayerPrefs.Save();
	}

   
		



}