﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GraffitiCleanerMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public int areaSizeX;
	public int areaSizeY;
	public Texture2D graffitiTexture;
	public Material graffitiMaterial;
	public MeshRenderer graffitiRenderer;
	public int radius;
	public float limitPercentage;
	public Image timerImage;
	public Texture2D[] graffitiTextures;
	public ParticleSystem particles;
	public AudioSource washingSound;

	private Camera m_cam;
	private Color m_transparent;
	private int m_amountOfPixelsInTenPercent;
	private RoundTimer m_timer;
	private Texture2D m_currentTexture;
	private bool m_isCompleted;

	void OnEnable()
	{
		m_timer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		m_timer.OnTimerEnd -= RoundEnd;
	}

	void Start()
	{
		Initialization();
	}

	public void Initialization()
	{
		GameManager.instance.DisableScoreBoard();
		m_cam = Camera.main;
		m_transparent = new Color(0f,0f,0f,0f);
		Material tag = new Material(graffitiMaterial);
		Texture2D pic = Instantiate(graffitiTextures[Random.Range(0, graffitiTextures.Length -1)]) as Texture2D;
		m_currentTexture = pic;
		m_amountOfPixelsInTenPercent = TenPercentsInPixels(m_currentTexture);
		tag.mainTexture = pic;
		graffitiRenderer.material = tag;
		int i = TenPercentsInPixels(pic);
		m_timer = GetComponent<RoundTimer>();
		m_timer.timerImage = timerImage;
		m_timer.OnTimerEnd += RoundEnd;
		StartCoroutine(m_timer.StartTimer(GameManager.instance.currentRoundDuration));
		StartCoroutine(counter());

		StartCoroutine(CheckIfTextureErasedEnough());
	}

	void Update()
	{
		EraseTexture();

		if(Input.GetButtonDown("Fire1"))
		{
			washingSound.Play();
		}

		if(Input.GetButtonUp("Fire1"))
		{
			washingSound.Stop();
		}
	}

	IEnumerator counter()
	{
		float limit = 3;
		float time = 0;

		while(true)
		{
			yield return null;
			time += Time.deltaTime;
			if(time > limit)
			{
				Texture2D tex = graffitiRenderer.material.mainTexture as Texture2D;
				CountNonTransparentPixels(tex);
				break;
			}
		}
	}

	void EraseTexture()
	{
		if (!Input.GetMouseButton(0))
			return;
		
		RaycastHit hit;
		if (!Physics.Raycast(m_cam.ScreenPointToRay(Input.mousePosition), out hit))
			return;
		
		Renderer rend = hit.transform.GetComponent<Renderer>();
		MeshCollider meshCollider = hit.collider as MeshCollider;
		if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
			return;
		
		Texture2D tex = rend.material.mainTexture as Texture2D;

		float xCordTemp = 0;
		float yCordTemp = 0;

		Vector2 pixelUv = hit.textureCoord;
		xCordTemp = pixelUv.x;
		yCordTemp = pixelUv.y;

		xCordTemp *= tex.width;
		yCordTemp *= tex.height;

		int xCord = (int)xCordTemp;
		int yCord = (int)yCordTemp;

		CircleCutOutFromTexture(xCord, yCord, tex, m_transparent);

		tex.Apply();

		var position = m_cam.ScreenToWorldPoint(Input.mousePosition);
		position.z = -1.0f;
		particles.transform.position = position;

		particles.Emit(1);
	}

	void CircleCutOutFromTexture(int cx, int cy, Texture2D tex, Color col)
	{
		int x, y, px, nx, py, ny, d;
		 
		 for (x = 0; x <= radius; x++)
		 {
			 d = (int)Mathf.Ceil(Mathf.Sqrt(radius * radius - x * x));
			 for (y = 0; y <= d; y++)
			 {
				 px = cx + x;
				 nx = cx - x;
				 py = cy + y;
				 ny = cy - y;
 
				 tex.SetPixel(px, py, col);
				 tex.SetPixel(nx, py, col);
  
				 tex.SetPixel(px, ny, col);
				 tex.SetPixel(nx, ny, col);
 
			 }
		 }    
	}

	int CountNonTransparentPixels(Texture2D tex)
	{
		Color32[] array = tex.GetPixels32();
		int pixes = 0;

		foreach(Color32 col in array)
		{
			if(col.a > 0f)
			{
				pixes++;
			}
		}

		return pixes;
	}

	int TenPercentsInPixels(Texture2D tex)
	{
		Color32[] array = tex.GetPixels32();
		int pixels = 0;

		for(int i = 0; i < array.Length; i++)
		{
			if(array[i].a > 0f)
			{
				pixels++;
			}
		}
		int tenPercent = Mathf.RoundToInt(pixels * 0.10f);
		return tenPercent;
	}

	public void RoundEnd()
	{
		if (!isCompleted) {
			isCompleted = true;
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	public bool IsCompleted()
	{
		int nonTransparentPixelsLeft = CountNonTransparentPixels(m_currentTexture);

		return nonTransparentPixelsLeft > m_amountOfPixelsInTenPercent ? false : true;
	}

	IEnumerator CheckIfTextureErasedEnough()
	{
		while(true)
		{
			yield return new WaitForSeconds(0.5f);

			int nonTransparentPixelsLeft = CountNonTransparentPixels(m_currentTexture);

			if(nonTransparentPixelsLeft < m_amountOfPixelsInTenPercent && !isCompleted)
			{
				GameManager.instance.RoundEndActions(true);
				isCompleted = true;
				break;
			}
				
		}
	}
}
