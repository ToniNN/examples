﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SandRoadMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public AudioSource sandSound;
	public Image timerImage;
	public Transform spawnPosition;


	private ShapeToucher m_shapeToucher;
	private RoundTimer m_timer;
	private bool m_completed;

	void OnEnable()
	{
		m_timer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		m_timer.OnTimerEnd -= RoundEnd;
	}

	void Awake()
	{
		Initialization();
	}

	public void Initialization()
	{
		m_shapeToucher = GetComponent<ShapeToucher>();

		InvokeRepeating("Checker", 0.5f, 0.2f);

		m_shapeToucher.SpawnShape(spawnPosition.position);

		m_timer = GetComponent<RoundTimer>();
		m_timer.timerImage = timerImage;
		m_timer.OnTimerEnd += RoundEnd;
		StartCoroutine(m_timer.StartTimer(GameManager.instance.currentRoundDuration));
	}

	public bool IsCompleted()
	{
		if(m_shapeToucher.allCollidersTouched)
			return true;
		else 
			return false;
	}


	public void RoundEnd()
	{
		if (!m_completed) {
			m_completed = true;
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	void Checker()
	{
		if(IsCompleted())
		{
			CancelInvoke();
			if (!m_completed) {
				m_completed = true;
				GameManager.instance.RoundEndActions (true);
			}
		}
	}

	void Update()
	{
		if(Input.GetButtonDown("Fire1"))
		{
			sandSound.Play();
		}

		if(Input.GetButtonUp("Fire1"))
		{
			sandSound.Stop();
		}
	}
}
