﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RakingMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public Image timerImage;
	public RoundTimer timer;
	public GameObject rake;

	private Vector3 m_newPos;
	private Vector3 m_offset;
	private Vector2 m_hitPoint;
	private Vector2 m_rakeStart;
	private bool m_gameCompleted;
	private int m_leavesLeft = 4;

	void OnEnable()
	{
		timer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		timer.OnTimerEnd -= RoundEnd;
	}

	void Awake ()
	{
		Initialization ();
	}

	public void Initialization()
	{
		timer = GetComponent<RoundTimer>();
		timer.timerImage = timerImage;
		StartCoroutine(timer.StartTimer(GameManager.instance.currentRoundDuration));
	}
		
	void Update()
	{
		if(!m_gameCompleted)
			Rake();
	}

	public Vector2 GetRakeStart ()
	{
		return m_rakeStart;
	}

	void Rake()
	{
		if (Input.GetButtonDown ("Fire1")) 
		{	
			m_rakeStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			SoundManager.instance.PlaySound(SoundManager.Sounds.Swoosh);
		}
		if (Input.GetButton ("Fire1")) 
		{	
			Vector3 touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector3 newPosition = new Vector3(touchPosition.x, touchPosition.y, 0);
			rake.transform.position = newPosition;
		}
	}

	void OnTriggerEnter2D(Collider2D leaf)
	{
		if (leaf.gameObject.tag == "Leaf") 
		{
			leaf.GetComponent<Rigidbody2D> ().drag = 20;
			leaf.gameObject.GetComponent<CircleCollider2D> ().enabled = false;
			m_leavesLeft--;
			SoundManager.instance.PlaySound(SoundManager.Sounds.LeafCollected);
		}
		if(m_leavesLeft <= 0 && !m_gameCompleted)
		{
			m_gameCompleted = true;
			GameManager.instance.RoundEndActions(true);
		}
	}

	public void RoundEnd()
	{
		if (!m_gameCompleted) {
			m_gameCompleted = true;
			GameManager.instance.RoundEndActions (false);
		}
	}

	public bool IsCompleted()
	{
		return false;
	}

}
