﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MapReferences : MonoBehaviour {

	public RectTransform marttiTransition, kupittaaTransition, keskustaTransition, satamaTransition;
	public Button kupittaaBackButton;
	public GameObject roundsEndedScreen;
	public TextMeshProUGUI roundsEndedScore;

	public GameObject tutorialCanvas;
	public GameObject ilmariAndJenna;
	public GameObject firstTutorialBubble;
	public GameObject secondTutorialBubble;
	public GameObject thirdTutorialBubble;
	public GameObject fourthTutorialBubble;
	public GameObject tutorialClouds;
	public TextMeshProUGUI firstTutorialBubbleText;

	public Button tutorialSecondBubbleButton;

	public Button[] mapButtons;

	public CanvasGroup tutorialCanvasGroup;

	public Animator shopButtonAnimator;
	public Animator keskustaGlowAnimator;

	public InputField feedbackText;


	public void OpenFeedbackPage()
	{
		Application.OpenURL("https://opaskartta.turku.fi/eFeedback/");
	}

	public void CloseCreditsWindow()
	{
		SoundManager.instance.PlaySound(SoundManager.Sounds.CloseShopSound);
	}

	public void OpenCreditsSound()
	{
		SoundManager.instance.PlaySound(SoundManager.Sounds.IcePickDestroyed);
	}
	
	


}
