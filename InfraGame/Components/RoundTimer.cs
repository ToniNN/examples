﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoundTimer : MonoBehaviour {

	//public variables for debugging and dependencies in Editor
	[HideInInspector]
	public Text timerText;
	public delegate void TimerEndAction();
	public event TimerEndAction OnTimerEnd; 
	public Image timerImage;

	private RoundDuration m_currentDuration;
	private float m_timeRemaining;
	private float m_roundTime;
	private bool m_animationTriggered;

	public enum RoundDuration
	{
		Twenty = 0,
		Fifteen = 1,
		Ten = 2,
		Five = 3,
		Fourteen = 4,
		Twelve = 5,
		Eight = 6
	};

	public float timeRemaining
	{
		get
		{
			return m_timeRemaining;
		}
	}

	public float timeAsT
	{
		get
		{
			return m_timeRemaining / m_roundTime;
		}
	}

	public IEnumerator StartTimer(RoundDuration duration)
	{
		if(timerImage == null)
		{
			Debug.LogError("No Image component set for the timer");
			this.StopAllCoroutines();
		}

		float time = 0;

		switch(duration)
		{
			case RoundDuration.Twenty:
				time = 10;
				m_roundTime = 20f;
				break;
			case RoundDuration.Fifteen:
				time = 7f;
				m_roundTime = 15f;
				break;
			case RoundDuration.Ten:
				time = 5f;
				m_roundTime = 10f;
				break;
			case RoundDuration.Five:
				time = 3f;
				m_roundTime = 5f;
				break;

			case RoundDuration.Fourteen:
				time = 14f;
				m_roundTime = 28f;
				break;
			case RoundDuration.Twelve:
				time = 12f;
				m_roundTime = 24F;
				break;
			case RoundDuration.Eight:
				time = 8f;
				m_roundTime = 16f;
				break;
		}

		while(time > 0)
		{
			time -= Time.deltaTime;
			m_timeRemaining = time;
			timerImage.fillAmount = timeAsT;

			if(timeAsT < 0.3 && !m_animationTriggered)
			{
				timerImage.GetComponent<Animator>().Play("closeToEnd");
				m_animationTriggered = true;
			}
			yield return null;
		}

		OnTimerEnd();


	}

	public void Stop()
	{
		StopAllCoroutines();
		timerImage.enabled = false;
	}







}
