﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObstacleJumpingMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public Image timerImage;
	public RoundTimer timer;
	public Transform endPoint;

	private Animator m_anim;
	private bool m_jumping;
	private bool m_walking;
	private bool m_gameCompleted;
	private bool m_failed;
	private Vector3 m_startPos;
	private float m_step = 1.5f;
	private float m_speed = 0f;

	void OnEnable()
	{
		timer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		timer.OnTimerEnd -= RoundEnd;
	}

	void Awake ()
	{
		Initialization ();
	}

	public void Initialization()
	{
		m_startPos = transform.parent.transform.position;

		RoundTimer.RoundDuration customDur = RoundTimer.RoundDuration.Twenty;
	
		timer = GetComponent<RoundTimer>();
		timer.timerImage = timerImage;
		StartCoroutine(timer.StartTimer(customDur));
		m_anim = GetComponent<Animator> ();
		m_jumping = false;
		m_walking = true;
	}


	void Update()
	{
		if(!m_gameCompleted)
			CheckForInput();

		if (m_walking){
			m_speed += Time.deltaTime*0.1f; 
			transform.parent.transform.position = Vector3.Lerp (m_startPos, new Vector3(50f, -1.11f, -2f), m_speed);
		}
	}

	void CheckForInput()
	{
		if (Input.GetButtonDown ("Fire1") && !m_jumping && m_walking) 
		{	
			m_anim.Play ("trash-jump");
			SoundManager.instance.PlaySound(SoundManager.Sounds.JennaJumps);
			m_jumping = true;
		}
	}

	void StartWalking () //kutsutaan animaatiosta trash-idle
	{ 
		m_walking = true;
		m_anim.Play ("trash-walk");
	}

	void StopJumping() //kutsutaan animaatiosta trash-jump
	{  
		m_jumping = false;
		if (m_failed)
			m_anim.Play ("trash-fall");
		if (m_walking)
			m_anim.Play ("trash-walk");
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Trash") 
		{
			other.GetComponent<BoxCollider2D> ().enabled = false;
			m_failed = true;
			if (m_jumping == false) 
			{
				m_anim.Play ("trash-fall");
				SoundManager.instance.PlaySound(SoundManager.Sounds.JennaFallsDown);
			}
			StartCoroutine(Delay(2.0f, false));
		} 
		else if (!m_gameCompleted)
		{
			m_anim.Play ("trash-idle");
			GameManager.instance.RoundEndActions (true);
			m_gameCompleted = true;
		}
		m_walking = false;
	}

	

	public void RoundEnd()
	{
		if (!m_gameCompleted) {
			m_gameCompleted = true;
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	public bool IsCompleted() 
	{
		return true;
	}

	IEnumerator Delay(float delay, bool completed)
	{
		yield return new WaitForSeconds(delay);
		if (!m_gameCompleted) {
			m_gameCompleted = true;
			GameManager.instance.RoundEndActions (false);
		}
	}
}
