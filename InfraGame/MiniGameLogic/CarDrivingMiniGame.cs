﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class CarDrivingMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public Transform car;
	public float distance;
	public float speed = 0.5f;
	public Transform[] spawnPoints;
	public GameObject obstacle;
	public RoundTimer timer;
	public BoxCollider2D obstaclesAvoidedCollider;
	public Image timerImage;
	public float obstacleSpeed;
	public float rotationSpeed;
	public AudioSource carSound;

	private float m_yPos;
	private int m_nextSpawn = 1;
	private bool m_spawnedOnRight;
	private bool m_isHit;
	private bool m_completed;
	private GameObject m_road;
	private List<GameObject> m_obstacles = new List<GameObject>();

	void OnEnable()
	{
		timer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		timer.OnTimerEnd -= RoundEnd;
	}

	void Start()
	{
		Initialization();
	}

	public void Initialization()
	{
		RoundTimer.RoundDuration customDur = RoundTimer.RoundDuration.Eight;
		GameManager.Round currentRound = GameManager.instance.currentRound;

		//timer
		switch (currentRound)
		{
		case GameManager.Round.First:
			customDur = RoundTimer.RoundDuration.Ten;
			break;

		case GameManager.Round.Second:
			customDur = RoundTimer.RoundDuration.Fifteen;
			break;

		case GameManager.Round.Third:
			customDur = RoundTimer.RoundDuration.Twenty;
			break;

		case GameManager.Round.Fourth:
			customDur = RoundTimer.RoundDuration.Fourteen;
			break;
		}

		timer = GetComponent<RoundTimer>();
		timer.timerImage = timerImage;
		timer.OnTimerEnd += RoundEnd;
		StartCoroutine(timer.StartTimer(customDur));

		bool isEditor = false;
		#if UNITY_EDITOR
		isEditor = true;
		#endif

		if (!isEditor)
			InvokeRepeating("Raycast", 0, 0.05f);
		
		InvokeRepeating("SpawnObstacles", 0, 1.5f);
		m_yPos = car.position.y;

		m_road = GameObject.FindWithTag ("Hole");
	}

	public bool IsCompleted()
	{
		return true;
	}

	public void RoundEnd()
	{
		CancelInvoke ();
		if (!m_completed)
		{
			m_completed = true;

			foreach (GameObject obs in m_obstacles)
			{
				obs.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, 0, 0);
			}

			m_road.GetComponent<ScrollingUVs> ().StopScrolling ();
			carSound.Stop();
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	//called every frame
	void Update()
	{
		Steering();
	}

	void Steering()
	{
		if(!m_isHit)
		{
			car.Translate(Input.acceleration.x * speed, 0 ,0);
			car.Rotate(0,0, Input.acceleration.x * rotationSpeed);

			if(car.transform.position.x > 5) car.position = new Vector3(5,0,0);
			if(car.transform.position.x < -5) car.position = new Vector3(-5,0,0);

			if(car.transform.position.y > m_yPos) car.position = new Vector3(car.position.x, m_yPos, 0);
			if(car.transform.position.y < m_yPos) car.position = new Vector3(car.position.x, m_yPos, 0);
		}
	}

	void Raycast()
	{
		Debug.DrawRay(car.position, Vector3.up * distance);

		RaycastHit2D hit;
		hit = Physics2D.Raycast(car.position, Vector3.up, distance);

		if(hit.collider != null)
		{
			if(hit.collider.tag == "Obstacle" && !m_completed)
			{
				RoundEnd();
			}
		}
	}

	void SpawnObstacles()
	{
		var obs = Instantiate(obstacle, spawnPoints[m_nextSpawn].transform.position , Quaternion.identity) as GameObject;
		obs.GetComponent<Rigidbody2D>().velocity = new Vector3(0, obstacleSpeed, 0);
		m_nextSpawn = Random.Range (0, 3);
		m_obstacles.Add (obs);
	}

}
