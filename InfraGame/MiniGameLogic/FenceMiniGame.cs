﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class FenceMiniGame : MonoBehaviour, IMiniGame {

    //public variables for debugging and dependencies in Editor
    public Transform fence1, fence2, fence3;
	public float speed;
	public Sprite fenceOneHole, fenceTwoHoles;
	public SpriteRenderer fenceRenderer;
	public Transform[] fenceCrackPositionsTwoHoles;
	public Transform fenceCrackPositionOneHole;
	public GameObject fencePrefab;
	public Image timerImage;

	private Transform m_currentlySelectedObject;
	private float m_originalRotation;
	private float m_originalTouchAngle;
	private List<Transform> m_fenceTransforms = new List<Transform>();	
	private int m_numberOfFencesFixed;
	private RoundTimer m_timer;
	private bool m_isCompleted;

	void OnEnable()
	{
		m_timer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		m_timer.OnTimerEnd -= RoundEnd;
	}

	void Awake()
	{
		Initialization();
	}

	public void Initialization()
	{
		m_timer = GetComponent<RoundTimer>();
		m_timer.timerImage = timerImage;
		StartCoroutine(m_timer.StartTimer(GameManager.instance.currentRoundDuration));
		
		if(GameManager.instance.currentRound == GameManager.Round.First || GameManager.instance.currentRound == GameManager.Round.Second)
		{
			fenceRenderer.sprite = fenceOneHole;
			InitiateOneHoleFence();
		}
		else
		{
			fenceRenderer.sprite = fenceTwoHoles;
			InitiateTwoHoleFence();
		}
	}

	
	void InitiateOneHoleFence()
	{
		var obj = Instantiate(fencePrefab, fenceCrackPositionOneHole.position, Quaternion.Euler(0,0, Random.Range(0, 360))) as GameObject;
		m_fenceTransforms.Add(obj.transform);
	}

	void InitiateTwoHoleFence()
	{
		for(int i = 0; i < fenceCrackPositionsTwoHoles.Length; i++)
		{
			var obj = Instantiate(fencePrefab, fenceCrackPositionsTwoHoles[i].position, Quaternion.Euler(0,0, Random.Range(0, 360))) as GameObject;
			m_fenceTransforms.Add(obj.transform);
		}
	}


	void Update()
	{
		TurningFence();
	}

	void TurningFence()
	{
		if (Input.GetButton ("Fire1")) 
		{	
			if(m_currentlySelectedObject == null)
			{
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit2D hit;
				hit = Physics2D.Raycast (ray.origin, ray.direction);
		
				if(hit.collider != null)
				{
					if(hit.collider.tag == "Fence" && m_currentlySelectedObject == null)
					{
						m_currentlySelectedObject = hit.collider.gameObject.transform;

						var objectPos = Camera.main.WorldToScreenPoint(m_currentlySelectedObject.position);
						m_originalRotation = m_currentlySelectedObject.eulerAngles.z;
						var temp = Input.mousePosition - objectPos;
						m_originalTouchAngle = Mathf.Atan2(temp.y, temp.x) * Mathf.Rad2Deg - 90;
					}
				}


			}

			if(m_currentlySelectedObject != null)
			{
				Turn();
			}
		}
		else if(Input.GetButtonUp("Fire1") && m_currentlySelectedObject != null)
		{
			m_currentlySelectedObject = null;
		}
	}

	void Turn()
	{
		if(m_currentlySelectedObject != null)
		{
			var mouse_pos = Input.mousePosition;
			mouse_pos.z = 0f; //The distance between the camera and object
			var object_pos = Camera.main.WorldToScreenPoint(m_currentlySelectedObject.position);
			mouse_pos.x = mouse_pos.x - object_pos.x;
			mouse_pos.y = mouse_pos.y - object_pos.y;
			var angle = Mathf.Atan2(mouse_pos.y, mouse_pos.x) * Mathf.Rad2Deg - 90;
			var tempAngle = m_originalRotation + angle - m_originalTouchAngle;
			m_currentlySelectedObject.rotation = Quaternion.AngleAxis(tempAngle , Vector3.forward);

			CheckAngle(m_currentlySelectedObject.eulerAngles.z);
		}
	}

	void CheckAngle(float angle)
	{
		if(angle < 26 && angle > 24)
		{
			m_currentlySelectedObject.tag = "notMovable";
			m_currentlySelectedObject = null;
			m_numberOfFencesFixed++;
			SoundManager.instance.PlaySound(SoundManager.Sounds.FenceFixed);
		}

		if(angle < 206 && angle > 204)
		{
			m_currentlySelectedObject.tag = "notMovable";
			m_currentlySelectedObject = null;
			m_numberOfFencesFixed++;
			SoundManager.instance.PlaySound(SoundManager.Sounds.FenceFixed);
		}

		if(m_numberOfFencesFixed == m_fenceTransforms.Count && !m_isCompleted)
		{
			m_isCompleted = true;
			GameManager.instance.RoundEndActions(true);
		}
	}

	public bool IsCompleted()
	{
		if (m_numberOfFencesFixed == m_fenceTransforms.Count)
			return true;
		else
			return false;
	}

	public void RoundEnd()
	{
		if (!m_isCompleted)
		{
			m_isCompleted = true;
			GameManager.instance.RoundEndActions(IsCompleted());
		}
	}


}
