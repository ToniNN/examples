﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;


public class HoleFillingMiniGame : MonoBehaviour, IMiniGame
{
	//public variables for debugging and dependencies in Editor
	public int amountOfHoles;
	public GameObject holePrefab;
	public BoxCollider2D spawnArea;
	public int maxTries;
	public Text debugText;
	public RoundTimer roundTimer;
	public Image timerImage;

	private Vector3 m_newPos;
	private Collider2D[] m_neighbours;
	private Dictionary<int, GameObject> m_hitObjects = new Dictionary<int, GameObject>();
	private bool m_isCompleted;

	void Awake()
	{
		Initialization();
	}

	public void Initialization()
	{
		SpawnHoles();

		GameManager.instance.DisableScoreBoard();
		roundTimer = GetComponent<RoundTimer>();
		roundTimer.OnTimerEnd += RoundEnd;
		roundTimer.timerImage = timerImage;
		StartCoroutine(roundTimer.StartTimer(GameManager.instance.currentRoundDuration));

		InvokeRepeating("Checker", 0.5f, 0.5f);
	}

  

	void SpawnHoles()
	{
		for (int i = 0; i < amountOfHoles; i++)
		{
			var hole = Instantiate(holePrefab, GetRandomSpawnPoint(spawnArea.bounds), Quaternion.identity) as GameObject;

			SpriteRenderer renderer = hole.GetComponent<SpriteRenderer>();
			renderer.sortingOrder = i + 2;
		}

	}

	Vector3 GetRandomSpawnPoint(Bounds bounds)
	{
		Vector3 prefabSize = holePrefab.GetComponent<SpriteRenderer>().bounds.size;
		Bounds currentBounds = bounds;

		float adjustedMinX = bounds.min.x + (prefabSize.x / 2);
		float adjustedMinY = bounds.min.y + (prefabSize.y / 2);
		float adjustedMaxX = bounds.max.x - (prefabSize.x / 2);
		float adjustedMaxY = bounds.max.y - (prefabSize.y / 2);

		for (int i = 0; i < maxTries; i++)
		{
			m_newPos = new Vector3(UnityEngine.Random.Range(adjustedMinX, adjustedMaxX), UnityEngine.Random.Range(adjustedMinY, adjustedMaxY), 0);

			if (CheckIfOtherObjectsNear(m_newPos, prefabSize.x * prefabSize.y))
			{
				continue;
			}
			else
			{
				return m_newPos;
			}
		}


		return m_newPos;

	}

	bool CheckIfOtherObjectsNear(Vector3 pos, float distance)
	{
		m_neighbours = Physics2D.OverlapCircleAll(pos, distance, 1 << 9);

		if (m_neighbours.Length > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void Update()
	{

		debugText.text = m_hitObjects.Count.ToString();

		if(Input.touchCount == 0)
		{
			m_hitObjects.Clear();
			return;
		}

		for(int i = 0; i < Input.touchCount; i++)
		{
			Touch touch = Input.touches[i];

			if(touch.phase == TouchPhase.Stationary)
			{
				Ray ray = Camera.main.ScreenPointToRay (touch.position);
				RaycastHit2D hit;
				hit = Physics2D.Raycast (ray.origin, ray.direction);

				if(hit.collider != null)
				{
					if(hit.collider.tag == "Hole" && m_hitObjects.ContainsValue(hit.collider.gameObject) == false && m_hitObjects.ContainsKey(i) == false)
					{
						m_hitObjects.Add(i, hit.collider.gameObject);
						SoundManager.instance.PlaySound(SoundManager.Sounds.HoleFilling);
					}
				}
				else if(hit.collider == null)
				{
					m_hitObjects.Remove(i);
				}
						
			}
			if(touch.phase == TouchPhase.Ended)
			{
				m_hitObjects.Remove(i);
			}
		}

	}

	bool CheckIfHolesAreFilled()
	{
		if(m_hitObjects.Count == amountOfHoles)
		{
			return true;
		}
		else
			return false;
	}

	void Checker()
	{
		if(!m_isCompleted && IsCompleted())
		{
			CancelInvoke();
			m_isCompleted = true;
			GameManager.instance.RoundEndActions(true);
		}
	}

	public bool IsCompleted()
	{
		if (CheckIfHolesAreFilled())
			return true;
		else
			return false;
	}

	public void RoundEnd()
	{
		if (!m_isCompleted)
		{
			m_isCompleted = true;
			GameManager.instance.RoundEndActions(IsCompleted());
		}
	}

}
