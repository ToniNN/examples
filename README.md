# README #

Examples of my work

# Infra - Pelasta Turku! #

The code files for the project I did for the City of Turku. The project was made with the Unity3D-engine and written in C#. The game is comprised of 20 minigames and a hub world of Turku City. Due to the limited scale of the project I implemented the minigames as their own classes which handle the per game logic and dependencies. Singleton pattern is used in the Manager-classes to handle global variables and top-level game logic. 

The game can be downloaded from the Google Play Store here: https://play.google.com/store/apps/details?id=com.tuiske.harava&hl=en

# Asteroid Attack exercise #

Small exercise game where the player can sling asteroids towards earth by pressing the left mouse button and draggin the desired direction vector. Made with Unity3D using C#. The point of the exercise was to focus on two mathematical problems:

1. To calculate if the asteroid will hit the safe zone that surrounds earth.
2. To calculate the velocity for the missile that intercepts these dangerous asteroids.

As the problem focused on the mathematical calculation of these problems, I didn't use the game engine tools to solve the issues. For example, the first problem would be trivial to solve by just placing a circle collider around the earth and raycasting a ray from each asteroid towards their direction vector and see if this ray intersects with the collider. Instead of this I solved the problem using trigonometric equations.