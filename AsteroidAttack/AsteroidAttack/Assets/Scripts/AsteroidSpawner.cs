﻿using UnityEngine;

/// <summary>
/// Spawns the asteroids and sets their line renderers
/// </summary>
public class AsteroidSpawner : MonoBehaviour {

	//public variables and references that can be set in the Editor
	public LineRenderer mouseLine;
	public float maxLineLenght;
	public GameObject asteroidPrefab;

	//private members
	Vector3 lineStartPosition;
	Vector3 lineEndPosition;

	//constants
	private const int RENDERER_VERTEX_COUNT = 2;
	private const float MIN_DRAG_DISTANCE = 0.3f;

	/// <summary>
	/// called at the start of the game
	/// </summary>
	void Start()
	{
		mouseLine.positionCount = RENDERER_VERTEX_COUNT;
	}

	/// <summary>
	/// Called every frame
	/// </summary>
	void Update()
	{
		Aiming();


		//if the player actually drags and not just clicks on the screen
		var dragDistance = Vector3.Distance(lineStartPosition, lineEndPosition);

		if (Input.GetMouseButtonUp(0) && dragDistance > MIN_DRAG_DISTANCE)
		{
			mouseLine.enabled = false;
			SpawnAsteroid();
		}
	}

	/// <summary>
	/// Handles the drag and aim functionality
	/// </summary>
	void Aiming()
	{
		if (Input.GetMouseButtonDown(0))
		{
			mouseLine.enabled = true;
			lineStartPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		}

		if (Input.GetMouseButton(0))
		{
			lineStartPosition.z = 0;

			mouseLine.SetPosition(0, lineStartPosition);

			lineEndPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			lineEndPosition.z = 0;

			mouseLine.SetPosition(1, lineEndPosition);
		}
	}

	/// <summary>
	/// Spawns the asteroid, sets its velocity and calls a method in the singleton
	/// </summary>
	void SpawnAsteroid()
	{
		//spawns an asteroid and sets the velocity based on the player input
		GameObject asteroid = Instantiate(asteroidPrefab, lineEndPosition, Quaternion.identity);
		var asteroidComponent = asteroid.GetComponent<Asteroid>();
		asteroidComponent.SetVelocity((lineStartPosition - lineEndPosition).normalized * DefenseSystem.instance.asteroidSpeed);

		//shows the line that the asteroids travels
		asteroid.GetComponent<Asteroid>().ShowTrajectory(asteroidComponent.velocity);

		//called to inform the defenseSystem about a spawned asteroid
		DefenseSystem.instance.SituationAnalysis(asteroid.transform.position, asteroidComponent.velocity, asteroid);
	}
	
}
