﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ToolPickingMiniGame : MonoBehaviour, IMiniGame {

	//public variables for debugging and dependencies in Editor
	public Image timerImage;
	public RoundTimer timer;
	public Text description;

	private int m_descriptionID;
	private int m_toolID;
	private bool m_failed;
	private bool m_isCompleted;

	void OnEnable()
	{
		timer.OnTimerEnd += RoundEnd;
	}

	void OnDisable()
	{
		timer.OnTimerEnd -= RoundEnd;
	}

	void Awake ()
	{
		Initialization ();
	}

	public void Initialization()
	{
		timer = GetComponent<RoundTimer>();
		timer.timerImage = timerImage;
		timer.OnTimerEnd += RoundEnd;
		StartCoroutine(timer.StartTimer(GameManager.instance.currentRoundDuration));
		m_descriptionID = Random.Range (1, 5);
		if (m_descriptionID == 1) //sakset
			description.text = "Näillä trimmataan pensaita.";
		if (m_descriptionID == 2) //hiekka
			description.text = "Tällä estetään liukastuminen.";
		if (m_descriptionID == 3) //rikkalapio
			description.text = "Näitä tarvitaan istutukseen.";
		if (m_descriptionID == 4) //roskakeppi
			description.text = "Tällä kerätään roskia.";
	}

	void Update ()
	{
		if (Input.GetButtonDown ("Fire1")) 
		{
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit2D hit;
			hit = Physics2D.Raycast (ray.origin, ray.direction);

			if (hit.collider != null) 
			{
				if (hit.collider.tag == "GardeningTool") 
				{
					m_toolID = (int)hit.transform.GetComponent<GardeningTool> ().tool;
					if (m_toolID == m_descriptionID && !m_isCompleted)
					{
						m_isCompleted = true;
						GameManager.instance.RoundEndActions (true);
					}
						
					else if (!m_isCompleted)
					{
						m_isCompleted = true;
						GameManager.instance.RoundEndActions(false);
					}
							
				}
			}
		}
	}

	public void RoundEnd() 
	{
		if (!m_isCompleted) {
			m_isCompleted = true;
			GameManager.instance.RoundEndActions (IsCompleted ());
		}
	}

	public bool IsCompleted() 
	{
		return false;
	}

}
